

package gobiguy.ggj3d;

/**
 *
 */
 


// temp
import idx3d.*;
import java.awt.*;
import java.awt.image.*;
import java.net.*;
import java.io.*;

public class ggj3dTexture /*temp extends idx3d_Texture*/
{
	int width;
	int height;
	int[] pixels;
	
	public ggj3dTexture(int c)
	{
		this(1, 1, c);
	}
	
	public ggj3dTexture(int w, int h, int c)
	{
		width = w;
		height = h;
		pixels = new int[w * h];
		for(int i = 0; i < pixels.length; i++)
			pixels[i] = c;
	}
	
	public ggj3dTexture(Image img)
	{
		this(0xffff00);
	}
	
	public ggj3dTexture(String filename) // app
	{
		try
		{
			loadTexture(Toolkit.getDefaultToolkit().getImage(new File(filename).getName()));
		}
		catch(Exception e)
		{
		}
	}
	
	public ggj3dTexture(URL docURL, String filename) // applet
	{
		int pos=0;
		String temp=docURL.toString();
		while (temp.indexOf("/",pos)>0) pos=temp.indexOf("/",pos)+1;
		temp=temp.substring(0,pos)+filename;
		
		try{
			loadTexture(Toolkit.getDefaultToolkit().getImage(new URL(temp)));
		}
		catch(Exception e)
		{
		}
	}
	
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public int[] getPixels()
	{
		return pixels;
	}
	
	private void loadTexture(Image img)
	// grabbs the pixels out of an image
	{
		try
		{
			while (((width=img.getWidth(null))<0)||((height=img.getHeight(null))<0));

			pixels=new int[width*height];
			PixelGrabber pg=new PixelGrabber(img,0,0,width,height,pixels,0,width);
			pg.grabPixels();
		}
		catch (InterruptedException e) {}
	}

	// Call from Applet
	/*
	int color;
	
	public ggj3dTexture(int c)
	{
		super(1, 1);
		color = c;
		pixel = null;
	}

	public ggj3dTexture(int w, int h)
	{
		super(w, h);
	}

	public ggj3dTexture(int w, int h, int data[])
	{
		super(w, h, data);
	}

	public ggj3dTexture(Image img)
	{
		super(img);
	}
	
	public ggj3dTexture(URL docURL, String filename)
	// Call from Applet
	{	
		super(docURL, filename);
	}		
	
	public ggj3dTexture(String filename)
	{
		super(filename);
	}
	
	//
	public int getColor(int x, int y)
	{
		if (pixel != null && x >= 0 && y >= 0 && x < width && y < height)
		{
			return pixel[y * width + x];
		}
		
		return color;
	}
	*/
}

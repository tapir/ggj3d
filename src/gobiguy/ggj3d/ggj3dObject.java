package gobiguy.ggj3d;

/**
 * ggj3dObject, contains many triangles and vertices
 */

import gobiguy.ggjmath.*;
import java.util.*;

public class ggj3dObject extends ggj3dComponent
{
	// 2 vectors
	Vector trianglesVector = new Vector();
	Vector verticesVector = new Vector();
	
	// material
	public ggj3dMaterial material;
	// texture
	public ggj3dTexture texture;

	//////////////////////////////
	// construction
	//////////////////////////////
	public ggj3dObject()
	{
		super();
		
	}
	
	////////////////////////////
	// add vertex and triangels
	////////////////////////////
	public void addTriangle(ggj3dTriangle triangle)
	{
		if( triangle.object == this ) return;
		trianglesVector.addElement(triangle);
		triangle.setObject(this);
		
		addVertex(triangle.p1); triangle.p2.registerTriangle(triangle);
		addVertex(triangle.p2); triangle.p3.registerTriangle(triangle);
		addVertex(triangle.p3); triangle.p1.registerTriangle(triangle);
		
		// add to scene
		if ( scene == null ) return;
		scene.registerTriangle(triangle);		
	}
	
	public void addVertex(ggj3dVertex vertex)
	{
		if( vertex.object == this ) return;
		verticesVector.addElement(vertex);
		vertex.setObject(this);
		
		//
		if ( texture != null )
		{
			vertex.tx = (int)(vertex.u * (float)(texture.width - 1));
			vertex.ty = (int)(vertex.v * (float)(texture.height - 1));
		}
		
		// add to scene
		if ( scene != null )
			scene.registerVertex(vertex);
	}
	
	/**
	 * set scene
	 */
	public void setScene(ggj3dScene s)
	{
		if( scene == s ) return;
		
		if ( scene == null && s != null)
		// add vertice and triangles to scene
		{
			addVerticesToScene(s);
			addTrianglesToScene(s);
		}
		else if ( scene != null && s == null)
		// remove vertice and triangles to scene
		{
			removeVerticesToScene(scene);
			removeTrianglesToScene(scene);
		}
		else if ( scene != null && s != null)
		// remove vertice and triangles to scene
		// add vertice and triangles to s
		{
			removeVerticesToScene(scene);
			removeTrianglesToScene(scene);
			addVerticesToScene(s);
			addTrianglesToScene(s);
		}
		
		// change scene
		super.setScene(s);
	
	}
	
	/**
	 * set material
	 */
	public void setMaterial(ggj3dMaterial m)
	{
		material = m;
	}
	
	public ggj3dMaterial getMaterial()
	{
		return material;
	}
	
	//
	public void setTexture(ggj3dTexture t)
	{
		texture = t;
		
		// cal absolute texture coordinate of vertice in this object 
		Enumeration enum = verticesVector.elements();
		ggj3dVertex vertex;
		while ( enum.hasMoreElements() )
		{
			vertex = (ggj3dVertex)(enum.nextElement());
			vertex.tx = (int)(vertex.u * (float)(texture.width - 1));
			vertex.ty = (int)(vertex.v * (float)(texture.height - 1));
		}
	}
	
	public ggj3dTexture getTexture()
	{
		return texture;
	}
	
	/**
	 * add vertices to scene
	 */
	public void addVerticesToScene(ggj3dScene s)
	{
		Enumeration enum;
		enum = verticesVector.elements();
		ggj3dVertex vertex;
		while ( enum.hasMoreElements() )
		{
			vertex = (ggj3dVertex)(enum.nextElement());
			s.registerVertex(vertex);
		}
		
	}
	
	/**
	 * remove vertices to scene
	 */
	public void removeVerticesToScene(ggj3dScene s)
	{
		Enumeration enum;
		enum = verticesVector.elements();
		ggj3dVertex vertex;
		while ( enum.hasMoreElements() )
		{
			vertex = (ggj3dVertex)(enum.nextElement());
			s.unRegisterVertex(vertex);
		}
		
	}
	
	/**
	 * add triangles to scene
	 */
	 public void addTrianglesToScene(ggj3dScene s)
	 {
		 Enumeration enum;
		 enum = trianglesVector.elements();
		 ggj3dTriangle triangle;
		 while ( enum.hasMoreElements() )
		 {
		 	triangle = (ggj3dTriangle)(enum.nextElement());
		 	s.registerTriangle(triangle);
		 }
	 }
	
	/**
	 * remove triangles to scene
	 */
	 public void removeTrianglesToScene(ggj3dScene s)
	 {
		 Enumeration enum;
		 enum = trianglesVector.elements();
		 ggj3dTriangle triangle;
		 while ( enum.hasMoreElements() )
		 {
		 	triangle = (ggj3dTriangle)(enum.nextElement());
		 	s.unRegisterTriangle(triangle);
		 }
	 }
	
	/**
	 * update matrices
	 */
	public void updateMatrices()
	{
		//System.out.println("enter object.updateMatrices()");
		
		// update matrices
		super.updateMatrices();
		//vertexMatrix.print();
		


		// transform verteice and triangles
		transform();
		
		//System.out.println("finish object.updateMatrices()");
	}
	
	/**
	 * transform vertices and triangles
	 */
	public void transform()
	{
		
		Enumeration enum;
		// transform vertice
		enum = verticesVector.elements();
		ggj3dVertex vertex;
		while ( enum.hasMoreElements() )
		{
			vertex = (ggj3dVertex)(enum.nextElement());
			vertexMatrix.transform(vertex.p, vertex.pos);
			normalMatrix.transform(vertex.n, vertex.normal);
		}
		
		// transform triangles			
		enum = trianglesVector.elements();
		ggj3dTriangle triangle;
		while ( enum.hasMoreElements() )
		{
			triangle = (ggj3dTriangle)(enum.nextElement());
			normalMatrix.transform(triangle.n, triangle.normal);
		}
	}	
}

package gobiguy.ggj3d;

/**
 * ggj3dScene, not only is a container, a scene has
 * environment
 */

import gobiguy.ggjmath.*;
import java.util.*;

public class ggj3dScene extends ggj3dContainer
{
	// the 2 variables are used to accelerate renderring
	// it seems that verticesVector and trianglesVector
	// can be removed if modify project function
	
	// all object in this scene
	//Hashtable objectsTable = new Hashtable(); // constant 
	
	// object wiil be rendered
	//Vector visibleObjectsVector = new Vector(); // not constant

	// all vertices in this scene
	Vector verticesVector = new Vector(); // constant

	// all triangels in this scene
	Vector trianglesVector = new Vector(); // constant

	// triangels wiil be rendered
	//Vector visibleTrianglesVector = new Vector(); // not constant

	// lights
	Vector lightsVector = new Vector();
	ggj3dLight[] lights;

	// bgcolor
	int bgColorValue = 0xffbbbbbb;
	
	// env
	public ggj3dEnvironment environment;
	
	/**
	 * set environment
	 */
	public void setEnvironment(ggj3dEnvironment e)
	{
		environment = e;
	}
	
	//
	public ggj3dEnvironment getEnvironment()
	{
		return environment;
	}
	
	public ggj3dLight[] /*Vector*/ /*Enumeration*/ getLights()
	{
		lights = new ggj3dLight[lightsVector.size()];
		Enumeration enum;
		enum = lightsVector.elements();
		int index = 0;
		while( enum.hasMoreElements() )
		{
			lights[index++] = (ggj3dLight)(enum.nextElement());
		}
		return lights;//.elements();
	}
	
	/**
	 * add component
	 */
	public void add(ggj3dComponent c)
	{
		// not implemented now
		/*
		ggj3dContainer ct = c.getScene();
		if ( ct != null )
		{
			ct.remove(c);
		}
		*/
		super.add(c);
		
		if ( c instanceof ggj3dLight )
		{
			if( ! lightsVector.contains(c) )
				registerLight((ggj3dLight)c);
		}
		
		//setSceneOfComponents(this);
		c.setScene(this);
	}
	
	/**
	 * add vertex to trianglesVector
	 */
	public void registerVertex(ggj3dVertex v)
	{
		verticesVector.addElement(v);
	}
	
	/**
	 * remove vertex to trianglesVector
	 */
	public void unRegisterVertex(ggj3dVertex v)
	{
		verticesVector.removeElement(v);
	}
	
	/**
	 * add triangle to trianglesVector
	 */
	public void registerTriangle(ggj3dTriangle tri)
	{
		trianglesVector.addElement(tri);
	}
	
	/**
	 * reomve triangle to trianglesVector
	 */
	public void unRegisterTriangle(ggj3dTriangle tri)
	{
		trianglesVector.removeElement(tri);
	}
	
	/**
	 * register light
	 */
	public void registerLight(ggj3dLight l)
	{
			lightsVector.addElement(l);
	}
	
	/**
	 * register light
	 */
	public void unRegisterLight(ggj3dLight l)
	{
			lightsVector.removeElement(l);
	}
	
	/**
	 * get visible triangles vector
	 */
	//public Vector getVisibleTrianglesVector()
	//{
	//	return visibleTrianglesVector;
	//}
	
	/**
	 * set background color
	 */
	public void setBgColor(int c)
	{
		bgColorValue = c;
	}
	
	/**
	 * get background color
	 */
	public int getBgColor()
	{
		return bgColorValue;
	}
	
	///
	void printV(String head, ggjVector v)
	{
		System.out.println(head + ": x=" + v.x + ", y=" + v.y + ",z=" + v.z + ",w=" + v.w);
	}

}

package gobiguy.ggj3d;

/**
 * ggj3dComponent, base class for object, scene, light, monitor, 
 * etc. It provides functions such as shifto\ing, rotating, ...
 */

import gobiguy.ggjmath.*;

public class ggj3dComponent
{
	// coordinate variables
	// the 4 vectors followed can specify a coordiante
	ggjVector pos = new ggjVector(0f, 0f, 0f);
	ggjVector u = new ggjVector(1f, 0f, 0f), 
				v = new ggjVector(0f, 1f, 0f), 
				w = new ggjVector(0f, 0f, 1f);
				
	// matrix used to transform a vertex or a normal to world or view
	ggjMatrix vertexMatrix = new ggjMatrix();  
	ggjMatrix normalMatrix = new ggjMatrix();  
	
	// container which contains this component
	ggj3dContainer container = null;
	
	// scene(root container) which contains this component
	ggj3dScene scene = null;
	
	// for dubeg
	String name = "";
	
	/**
	 * constuction functions
	 */
	public ggj3dComponent()
	{
	}
	
	//////////////////////////////////////////
	// set functions
	//////////////////////////////////////////
	/**
	 * set position of coordinate
	 */
	public void setPosition(ggjVector p)
	{
		pos.set(p);
	}
	
	/**
	 * set axis of coordiante
	 */
	public void setAxises(ggjVector v1, ggjVector v2, ggjVector v3)
	{
		u.set(v1); u.normalize();
		v.set(v2); v.normalize();
		w.set(v3); w.normalize();
	}
	
	/**
	 * set container
	 */
	public void setContainer(ggj3dContainer c)
	{		
		if( container == c ) return;
		
		// change container and scene
		if ( container == null && c != null )
		{
			setScene(c.getScene());
		}
		else if ( container != null && c == null )
		{
			container.remove(this);
			setScene(null);
		}
		else if ( container != null && c != null )
		{
			container.remove(this);
			c.add(this);
			setScene(c.getScene());
		}
		
		container = c;
	}
	
	/**
	 * set scene
	 */
	public void setScene(ggj3dScene s)
	{
		scene = s;
	}
	
	////////////////////////////////////////////////
	// get functions
	////////////////////////////////////////////////
	/**
	 * get vertex matrix
	 */
	public ggjMatrix getVertexMatrix()
	{
		return vertexMatrix;
	}
	
	/**
	 * get normal matrix
	 */
	public ggjMatrix getNormalMatrix()
	{
		return normalMatrix;
	}
	
	/**
	 * get container
	 */
	public ggj3dContainer getContainer()
	{
		return container;
	}
	
	/**
	 * get scene
	 */
	public ggj3dScene getScene()
	{
		return scene;
	}
	
	/////////////////////////////////////
	// transform function
	/////////////////////////////////////
	public void shift(float dx, float dy, float dz)
	{
		pos.shift(dx, dy, dz);
	}
	
	public void shiftTo(float x, float y, float z)
	{
		pos.shiftTo(x, y, z);
	}
	
	public void shiftSelf(float dx, float dy, float dz)
	{
		ggjVector dv = new ggjVector();
		if (dx != 0f)
		{
			dv.set(u);
			dv.scale(dx);  // right ?
			pos.shift(dv);
		}
		if (dy != 0f)
		{
			dv.set(v);
			dv.scale(dy);
			pos.shift(dv);
		}
		if (dz != 0f)
		{
			dv.set(w);
			dv.scale(dz);
			pos.shift(dv);
		}
	}

////////	
/*
	public void rotate(float dalpha, float dbeta, float dgamma)
	{
		pos.rotate(dalpha, dbeta, dgamma);
	}
*/
// change to

	public void rotatePosition(float dalpha, float dbeta, float dgamma)
	{
		pos.rotate(dalpha, dbeta, dgamma);
	}

////////////////////

	
	public void rotateSelf(float dalpha, float dbeta, float dgamma)
	{
		u.rotate(dalpha, dbeta, dgamma);
		v.rotate(dalpha, dbeta, dgamma);
		w.rotate(dalpha, dbeta, dgamma);
		/*
		ggjMatrix m1 = new ggjMatrix();
		m1.rotate(dalpha, dbeta, dgamma);
		ggjMatrix m2 = new ggjMatrix();
		m2.set(u, v, w);
		m2.multiply(m1);
		u.set(1f, 0f, 0f, 1f);
		v.set(0f, 1f, 0f, 1f);
		w.set(0f, 0f, 1f, 1f);
		m2.transform(u); u.normalize();
		m2.transform(v); v.normalize();
		m2.transform(w); w.normalize();
	*/
	}

////////////////
/*	
	public void rotateInContainer(float dalpha, float dbeta, float dgamma)
	{
		rotate(dalpha, dbeta, dgamma);
		rotateSelf(dalpha, dbeta, dgamma);
	}
*/
// change to

	public void rotate(float dalpha, float dbeta, float dgamma)
	{
		rotatePosition(dalpha, dbeta, dgamma);
		rotateSelf(dalpha, dbeta, dgamma);
	}

/////////////////////////	
	
	
	////////////////////////////////////////////////
	// update functions
	////////////////////////////////////////////////
	/**
	 * update matrices
	 */
	public void updateMatrices()
	{
		//System.out.println("enter component:" + name + "'s updateMatrices().");
		vertexMatrix.set(u, v, w, pos); 
		//vertexMatrix.print();
		normalMatrix.set(u, v, w); 
		if ( container != null )
		{
			vertexMatrix.multiplied(container.getVertexMatrix());
			normalMatrix.multiplied(container.getNormalMatrix());
		}
		
		//System.out.println("finish component:" + name + "'s updateMatrices.");
	}
	
	/**
	 * trace matrices
	 */
	public void traceMatrices()
	{
		//System.out.println("enter component:" + name + "'s updateMatrices().");
		vertexMatrix.set(u, v, w, pos); 
		//vertexMatrix.print();
		normalMatrix.set(u, v, w); 
		if ( container != null )
		{
			container.traceMatrices();
			vertexMatrix.multiplied(container.getVertexMatrix());
			normalMatrix.multiplied(container.getNormalMatrix());
		}
		
		//System.out.println("finish component:" + name + "'s updateMatrices.");
	}
	
	
	// only for debug
	public void setName(String n)
	{
		name = new String(n);
	}
	
	public String getName()
	{
		return name;
	}
	
}

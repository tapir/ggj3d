package gobiguy.ggj3d;

/**
 * ggj3dContainer, base class for component which can contains
 * object, scene, light, monitor, etc. <br>
 */

import gobiguy.ggjmath.*;
import java.util.*;

public class ggj3dContainer extends ggj3dComponent
{
	// hash table contains some componentsVector
	Vector componentsVector = new Vector();
	
	/**
	 * constrution function
	 */
	public ggj3dContainer()
	{
	}
	
	/**
	 * set scene
	 */
	public void setScene(ggj3dScene s)
	{
		if( scene == s ) return;
		
		// change scene of self and components
		super.setScene(s);
		setSceneOfComponents(s);
	}
	
	/**
	 * set components' scene
	 */
	public void setSceneOfComponents(ggj3dScene s)
	{
		Enumeration enum;
		enum = componentsVector.elements();
		while ( enum.hasMoreElements() )
		{
			((ggj3dComponent)(enum.nextElement())).setScene(s);
		}
	}
	
	/**
	 * add component
	 */
	public void add(ggj3dComponent c)
	{
		// not allow add a scene to a container
		if( c instanceof ggj3dScene ) return;
		
		// romove from old container
		if( c.getContainer() != null )
			c.getContainer().remove(c);
		
		// not allow more than one copy in vector
		if( componentsVector.contains(c) ) return;
		
		// do other thing
		componentsVector.addElement(c);
		c.setContainer(this);
		
		// set components scene
		if( scene == null ) return;
		
		setSceneOfComponents(scene);
	}
	
	/**
	 * remove component
	 */
	public void remove(ggj3dComponent c)
	{
		componentsVector.removeElement(c);
		c.setContainer(null);
	}
	
	/**
	 * update matrices of componentsVector
	 */
	private void updateMatricesOfComponents()
	{
		Enumeration enum;
		enum = componentsVector.elements();
		while ( enum.hasMoreElements() )
		{
			((ggj3dComponent)(enum.nextElement())).updateMatrices();
		}
	}
	
	/**
	 * update matrices
	 */
	public void updateMatrices()
	{
		//System.out.println("enter container:" + name + "'s updateMatrices().");
		// self
		super.updateMatrices();
		//vertexMatrix.print();
		
		// componentsVector
		updateMatricesOfComponents();
		//System.out.println("finish container:" + name + "'s updateMatrices.");
	}
	
	/**
	 * update matrices, not same as super function
	 */
	public void updateMatrices(ggjMatrix vm, ggjMatrix nm)
	{
		// self
		super.updateMatrices();

		vertexMatrix.multipliedInv(vm);
		normalMatrix.multipliedInv(nm);
		//vertexMatrix.print();
		//vertexMatrix.print();
		
		// componentsVector
		updateMatricesOfComponents();
	}
	
	
}

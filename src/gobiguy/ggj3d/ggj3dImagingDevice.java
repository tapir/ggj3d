package gobiguy.ggj3d;

/**
 * ggj3dScene, not only is a container, a scene has
 * environment
 */

import java.util.*;

public abstract class ggj3dImagingDevice
{
	ggj3dScene scene;
	ggj3dMonitor monitor;

	public void render(ggj3dScene s)
	{
		scene = s;
	}
	
	public void setMonitor(ggj3dMonitor m)
	{
		monitor = m;
	}
}
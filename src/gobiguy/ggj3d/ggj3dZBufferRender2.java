package gobiguy.ggj3d;

/**
 * ggj3dZBufferRender, a scene render using z-buffer mothod
 */

import java.util.*;
import gobiguy.ggjmath.*;

public abstract class ggj3dZBufferRender2 extends ggj3dSceneRender
{	
	// z buffer which length equals the length of renderPixels
	int[] zBuffer = new int[0];
	
	// triangels wiil be rendered
	Vector visibleTrianglesVector = new Vector(); // not constant

	//
	ggj3dVertex p1, p2, p3; // 3 vertice of a triangle
	ggj3dVertex p;          // temp vertex
	
	// some temp variables
	int 
		x, dx, x1, x2, x3, x4, 
		y, dy, y1, y2, y3, y4,
		z, dz, z1, z2, z3, z4,
		xL, dxL, xR, dxR, xStart, xEnd,
		zL, dzL;
		
	float 
		nx, dnx, nx1, nx2, nx3, nx4,
		ny, dny, ny1, ny2, ny3, ny4,
		nz, dnz, nz1, nz2, nz3, nz4,
		nxL, dnxL,
		nyL, dnyL,
		nzL, dnzL;
		

	int 
		tx, dtx, tx1, tx2, tx3, tx4,
		ty, dty, ty1, ty2, ty3, ty4,
		txL, dtxL,
		tyL, dtyL,
		
		tW, tH,
		tInd, dtInd,
		tIndL, dtIndL,
		tVal;		

	int
		r, dr, r1, r2, r3, r4,		
		g, dg, g1, g2, g3, g4,		
		b, db, b1, b2, b3, b4,
		rL, drL,
		gL, dgL,
		bL, dbL;		

	//
	int ratio, tmp;		
	float ratiof;
	float cos;
	int color;
	ggjVector lightLine = new ggjVector();
	int offset, index;
	
	//
	Enumeration enum;
	ggj3dTriangle triangle;
	ggj3dVertex vertex;
	ggj3dObject object;
	ggj3dTexture texture; 
	ggjVector pos;
	
	//
	long ZBufferRender2_t;
	
	// render scene
	void render()
	{
		// clear zBuffer
		if( zBuffer.length != renderPixels.length )
			zBuffer = new int[renderPixels.length];
		for(int i = 0; i < zBuffer.length; i++)
			zBuffer[i] = 0xfffffff;
			
		// project triangles in scene to monitor coodinate
		projectSceneToMonitor();
		
		// render visible triangles
		enum = visibleTrianglesVector.elements();
		while ( enum.hasMoreElements() )
		{
			triangle = (ggj3dTriangle)(enum.nextElement());
			
			// task of sub class
			renderTriangle(triangle);
		}
			
	}
	
	//////////////////////////////////////////
	// sub class must implements this function
	//////////////////////////////////////////
	abstract void renderTriangle(ggj3dTriangle triangle);
	
	// cal light density of a vertex
	// return value in r, g, b
	void calLightDensity(ggj3dVertex v)
	{
		float ka = material.getAmbient();
		float kd = material.getDiffuse();
		float ks = material.getSpecular();
		float pwr = material.getPower();
		float len, kd_cos;
		r = 0; g = 0; b = 0;
		
		// contribution of env 
		color = environment.getLightColor();
		r += (int)(ka * (float)((color >> 16) & 0xff));
		g += (int)(ka * (float)((color >> 8) & 0xff));
		b += (int)(ka * (float)((color) & 0xff));
		

		// consider attenuation caused by the distance 
		//from monitor to this vertex		
	//	len = v.pos.getLength();
	//	if( len > 3f )
	//	kd /= (1f + 0.3f * (len - 3f) + 0.03f * (len - 3f) * (len - 3f));

		// contribution of all lights
		for(int i = 0; i < lights.length; i++)
		{
			light = lights[i];
			color = light.getColor();
			lightLine.set(v.pos, light.getPosition());
		//	len = 	lightLine.getLength();
			lightLine.normalize();
			cos = lightLine.dotProduct(v.normal);
			if( cos <= 0 ) continue;
			
			kd_cos = kd * cos;
			
			// consider attenuation caused by the distance 
			//from monitor to this vertex	
			//	if( len > 3f )
			//	kd_cos /= (1f + 0.3f * (len - 3f) + 0.03f * (len - 3f) * (len - 3f));

			// ...
			r += (int)(kd_cos * (float)((color >> 16) & 0xff));
			g += (int)(kd_cos * (float)((color >> 8 ) & 0xff));
			b += (int)(kd_cos * (float)((color      ) & 0xff));
		}
		
		// confine the values
		if( r > 255 ) r = 255;
		if( g > 255 ) g = 255;
		if( b > 255 ) b = 255;
	}
	
	/**
	 * project vertices
	 */
	void projectSceneToMonitor() 
	{
		int halfw = renderWidth >> 1;
		int halfh = renderHeight >> 1;
		float r = 0.5f * lenFocus * renderWidth / screenWidth;
		
		float invz;
		// project vertices and cal clipCode
		enum = (scene.verticesVector).elements();
		while ( enum.hasMoreElements() )
		{
			vertex = (ggj3dVertex)(enum.nextElement());
			pos = vertex.pos;
			invz = 1f / (pos.z > lenFocus ? pos.z : lenFocus);
			vertex.x = (int)( r * pos.x * invz) + halfw;
			vertex.y = (int)(-r * pos.y * invz) + halfh;
			vertex.z = (int)(65536f *  pos.z);
			
			// cal clipCode
			vertex.clipCode = 0;
			if (vertex.x < 0) vertex.clipCode |= 1;
			if (vertex.x >= renderWidth) vertex.clipCode |= 2;
			if (vertex.y < 0) vertex.clipCode |= 4;
			if (vertex.y >= renderHeight) vertex.clipCode |= 8;
			if (pos.z < lenFocus /* or 0 ? */) vertex.clipCode |= 16;
		}
		
		visibleTrianglesVector.removeAllElements();
		
		// project vertices and cal clipCode
		enum = (scene.trianglesVector).elements();
		while ( enum.hasMoreElements() )
		{
			triangle = (ggj3dTriangle)(enum.nextElement());
			triangle.visibility = true;
			
			if ( (triangle.p1.clipCode & triangle.p2.clipCode & triangle.p3.clipCode) != 0)
			//if ( (triangle.p1.clipCode | triangle.p2.clipCode | triangle.p3.clipCode) != 0)
				triangle.visibility = false;
			else if(ggjVector.dotProduct(triangle.normal, triangle.p1.pos) >= 0)
				triangle.visibility = false;
			if (triangle.visibility)
			{
				visibleTrianglesVector.addElement(triangle);
			}
		}
		
	}

	
}
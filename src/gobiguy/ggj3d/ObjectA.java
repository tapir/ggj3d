package gobiguy.ggj3d;

/**
 * ggj3dObject, contains many triangles and vertices
 */

import gobiguy.ggjmath.*;


public class ObjectA extends ggj3dObject
{
	public ObjectA()
	{
		super();

		ggj3dVertex vertex0 = new ggj3dVertex(0f, 0f, 0f);
			vertex0.setUV(0, 0);
		ggj3dVertex vertex1 = new ggj3dVertex(4f, 0f, 0f);
			vertex1.setUV(1, 0);
		ggj3dVertex vertex2 = new ggj3dVertex(4f, 4f, 0f);
			vertex2.setUV(1, 1);
		ggj3dVertex vertex3 = new ggj3dVertex(0f, 4f, 0f);
			vertex3.setUV(0, 1);
		ggj3dTriangle triangle0 = new ggj3dTriangle(vertex0, vertex1, vertex2);
		ggj3dTriangle triangle1 = new ggj3dTriangle(vertex0, vertex2, vertex3);
		
		addTriangle(triangle0);
		addTriangle(triangle1);
		
	}
}

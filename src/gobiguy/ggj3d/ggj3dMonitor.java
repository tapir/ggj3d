
package gobiguy.ggj3d;

/**
 * ggj3dObject, contains many triangles and vertices
 * now, the montor is focus-constant monitor
 */

import gobiguy.ggjmath.*;
import java.awt.*;
import java.awt.image.*;

public class ggj3dMonitor extends ggj3dComponent
{
	// 
	ggj3dSceneRender sceneRender = null;
	ggj3dPhoto currentPhoto = null;

	// inherenct attributes of len
	float lenFocus; // lenFocus of len
	float horizontalFOV; // horizontal Field of View
	float verticalFOV; // vertical Field of View
	
	float screenWidth; // = lenLength * tan(horizontalFOV / 2)
	float screenHeight; // = lenLength * tan(verticalFOV / 2)


	public ggj3dMonitor()
	{
		this(0.1f, 90f, 90f);
	}
	
	public ggj3dMonitor(float f, float hfov, float vfov)
	{
		setLenFocus(f);
		setLenFOV(hfov, vfov);
	}
	
	/////////////////////////////////
	// set and get attributes
	/////////////////////////////////
	public void setLenFocus(float focus)
	{
		lenFocus = focus;
	}
	
	public float getLenFocus()
	{
		return lenFocus;
	}
	
	public void setLenFOV(float hfov, float vfov)
	{
		horizontalFOV = hfov;
		verticalFOV = vfov;
		
		screenWidth = (float)(lenFocus * Math.tan(0.5d * ggjMath.degreeToRadian(horizontalFOV)));
		screenHeight = (float)(lenFocus * Math.tan(0.5d * ggjMath.degreeToRadian(verticalFOV)));
		
		if ( sceneRender != null )
		{
			sceneRender.setMonitor(this);
		}
	}
	
	public float getHorizontalFOV()
	{
		return horizontalFOV;
	}
	
	public float getVerticalFOV()
	{
		return verticalFOV;
	}
	
	public float getScreenWidth()
	{
		return screenWidth;
	}
	
	public float getScreenHeight()
	{
		return screenHeight;
	}
	
	/**
	 * set photo
	 */
	public void setCurrentPhoto(ggj3dPhoto p)
	{
		currentPhoto = p;
	}
	
	/**
	 * get photo
	 */
	public ggj3dPhoto getCurrentPhoto()
	{
		return currentPhoto;
	}
	
	/**
	 *
	 */
	public void setSceneRender(ggj3dSceneRender sr)
	{
		sceneRender = sr;
		if(sceneRender != null)
			sceneRender.setMonitor(this);		
	}
	
	/**
	 * shoot a scene
	 */
	public void shoot(ggj3dScene scene)
	{
		if( sceneRender == null ) return;
		sceneRender.render(scene);
	}
}
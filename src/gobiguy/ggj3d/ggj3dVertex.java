
package gobiguy.ggj3d;

/**
 * ggj3dVertex, vertex of triangle
 */

import gobiguy.ggjmath.*;


public class ggj3dVertex
{
	public ggjVector p = new ggjVector(); // in self
	public ggjVector pos = new ggjVector(); // in world or view
	ggjVector tempn = new ggjVector(); // in self, temp, may remove later
	public ggjVector n = new ggjVector(); // in self
	public ggjVector normal = new ggjVector(); // in world or view
	
	public float u, v; // for texture, relative, from 0 to 1
	public int tx, ty;  // for texture, absolute
	
	public /*float*/int x, y, z, w; // in view, projection
	public int nx, ny, nz; // 
	
	public ggj3dObject object;
	public int clipCode = 0;
	
	int triangles = 0;
	
	///////////////////////
	// constuction finction
	///////////////////////
	public ggj3dVertex(float x, float y, float z)
	{
		p.set(x, y, z, 1f);
	}
	
	public ggj3dVertex(ggjVector v)
	{
		p.set(v);
	}
	
	public ggj3dVertex(ggj3dVertex vertex)
	{
		p.set(vertex.p);
	}
	
	public void setObject(ggj3dObject o)
	{
		object = o;
	}
	
	public ggj3dObject getObject()
	{
		return object;
	}
		
	public void setUV(float u, float v)
	{
		if(u < 0) u = 0;
		if(u > 1) u = u - (float)((int)u);
		this.u = u;
		if(v < 0) v = 0;
		if(v > 1) v = v - (float)((int)v);
		this.v = v;
	}
	
	
	///////////////////////////////////////////////////////////
	// before renderring, vertex.n.normalize() muset be called.
	///////////////////////////////////////////////////////////
	public void registerTriangle(ggj3dTriangle tri)
	{
		/*
		n.scale( (float)triangles );
		n.add(tri.n);
		triangles++;
		n.scale( 1f / (float)triangles );
		n.normalize();
		*/
		tempn.add(tri.n);
		n.set(tempn);
		n.normalize();
	}
}

package gobiguy.ggj3d;

/**
 * ggj3dPhoto
 */

public class ggj3dPhoto
{
	int width;
	int height;
	int[] pixels;
	
	int bgColorValue;
	
	public ggj3dPhoto(int w, int h)
	{
		setSize(w, h);
		setBgColor(0x0);
	}
	
	public void setSize(int w, int h)
	{
		width = w;
		height = h;
		pixels = new int[w * h];
	}
	
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public int[] getPixels()
	{
		return pixels;
	}
	
	public void setBgColor(int c)
	{
		bgColorValue = c;
	}
	
	public int getBgColor()
	{
		return bgColorValue;
	}
}
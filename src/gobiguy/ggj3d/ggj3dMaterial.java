


package gobiguy.ggj3d;

/**
 * Material
 */
 


public class ggj3dMaterial
{
	// color, if a object has texture, this is no use
	//int color; // 0xrrggbb, components reflectivity 
				// rr / 256 : red reflectivity
				// gg / 256 : green reflectivity
				// bb / 256 : blue reflectivity
	
	// diffuse
	float ambient; // env global reflectivity
	float diffuse; // diffuse global reflectivity
	
	// specular
	float specular; // high light global reflectivity
	float power;	// high light reflect power
	
	//
	public ggj3dMaterial(/*int c,*/ float a, float d, float s, float p)
	{
		//color = c;
		ambient = a;
		diffuse = d;
		specular = s;
		power = p;
	}
	
	//
	public float getAmbient()
	{
		return ambient;
	}
	
	//
	public float getDiffuse()
	{
		return diffuse;
	}
	
	//
	public float getSpecular()
	{
		return specular;
	}
	
	//
	public float getPower()
	{
		return power;
	}
}

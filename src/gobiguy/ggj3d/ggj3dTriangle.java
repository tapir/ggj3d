
package gobiguy.ggj3d;

/**
 * ggj3dVertex, vertex of triangle
 */

import gobiguy.ggjmath.*;



public class ggj3dTriangle
{
	public ggj3dVertex p1, p2, p3;
	public ggjVector n = new ggjVector(); // normal in parent space
	public ggjVector normal = new ggjVector(); // in world or view space
	
	public ggj3dObject object;
	public boolean visibility = true;
	
	///////////////////////
	// constuction finction
	///////////////////////
	public ggj3dTriangle(ggj3dVertex p1, ggj3dVertex p2, ggj3dVertex p3)
	{
		setVertice(p1, p2, p3);
	}
	
	/////////////////////////
	// set functions
	/////////////////////////
	public void setVertice(ggj3dVertex p1, ggj3dVertex p2, ggj3dVertex p3)
	{
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		
		updateNormal();
	}
	
	public void setObject(ggj3dObject o)
	{
		object = o;
	}
	
	public ggj3dObject getObject()
	{
		return object;
	}
	
	///////////////////////////////
	// get functions
	///////////////////////////////
	
	
	///////////////////////////
	// 
	///////////////////////////
	public void updateNormal()
	{
		ggjVector v1 = new ggjVector(p1.p, p2.p);
		ggjVector v2 = new ggjVector(p1.p, p3.p);
		ggjVector.forkProduct(v1, v2, n);
		n.normalize();
	}
}

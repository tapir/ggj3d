package gobiguy.ggj3d;

/**
 * ggj3dSceneRender, for scene renderring
 */

import java.util.*;

public abstract class ggj3dSceneRender
{
	// scene
	ggj3dScene scene;
	
	ggj3dEnvironment environment;
	ggj3dLight[] lights;
	ggj3dLight light;
	
	// object
	ggj3dObject object;
	ggj3dMaterial material;
	ggj3dTexture texture;
	int[] texturePixels;
	
	// monitor
	ggj3dMonitor monitor;
	float screenWidth;
	float screenHeight;
	float lenFocus;
	
	// photo
	ggj3dPhoto photo;
	int photoWidth;
	int photoHeight;
	int[] photoPixels;
	
	// render pixels
	int[] renderPixels = new int[0];
	int renderWidth;
	int renderHeight;
	
	// real sise of photo
	int realWidth;
	int realHeight;
	int offsetX;
	int offsetY;
	
	// used to statics fps
	int  staticsFrames = 30;
	int  frameNumber = 0;
	long firstFrameTime;
	long lastFrameTime;
	float renderFPS = 0;
	
	// if antilias
	boolean needAntialias = false;

	/////////////////////////////////////////////////////
	// render scene
	/////////////////////////////////////////////////////
	public void render(ggj3dScene s)
	{
		// cal fps
		calRenderFPS();		
		
		
		/////////////////////////
		// prepare
		/////////////////////////
		
		// env and lights
		scene = s;
		if( scene == null ) return;
		environment = scene.getEnvironment();
		if( environment == null ) return;
		lights = scene.getLights();
		
		// monitor
		if( monitor == null ) return;
		screenWidth = monitor.getScreenWidth();
		screenHeight = monitor.getScreenHeight();
		lenFocus = monitor.getLenFocus();

		// photo
		photo = monitor.getCurrentPhoto();
		if( photo == null ) return;
		photoWidth = photo.getWidth();
		photoHeight = photo.getHeight();
		photoPixels = photo.getPixels();
		
		// real size
		float rx = (float)photoWidth / screenWidth;
		float ry = (float)photoHeight / screenHeight;
		float r = rx < ry ? rx : ry;
		realWidth = (int)(r * screenWidth);
		realHeight = (int)(r * screenHeight);
		offsetX = photoWidth - realWidth >> 1;
		offsetY = photoHeight - realHeight >> 1;
		
		// set render pixels attributes
		if ( needAntialias )
		{
			renderWidth = realWidth << 1;
			renderHeight = realHeight << 1;
		}
		else
		{
			renderWidth = realWidth;
			renderHeight = realHeight;
		}
		
		if ( renderPixels.length != renderWidth * renderHeight )
		{
			renderPixels = new int[ renderWidth * renderHeight ];
		}
		
		// transform scene to screen space
		monitor.traceMatrices();
		scene.updateMatrices(monitor.getVertexMatrix(), monitor.getNormalMatrix());

		// clear pixels
		clearPixels();
		
		// task of sub class....
		render();
		
		// cal photo pixels
		producePhotoPixels();
	}
	
	//////////////////////////////////////
	// sub class must implements this function
	//////////////////////////////////////
	abstract void render();
	
	
	// set atialias
	public void setAntialias(boolean al)
	{
		needAntialias = al;
	}

	// get atialias
	public boolean setAntialias()
	{
		return needAntialias;
	}

	// set owner
	public void setMonitor(ggj3dMonitor m)
	{
		monitor = m;
	}
	
	// get fps
	public float getRenderFPS()
	{
		return renderFPS;
	}
	
	///////////////////////////////////////////
	// protected functions
	
	// clear pixels
	void clearPixels()
	{
		int c1 = scene.getBgColor();
		int c2 = photo.getBgColor();
		int row, col;
		int index = 0;
		int right = photoWidth - offsetX;
		int bottom = photoHeight - offsetY;
		
		// this clip need optimized
		for(row = 0; row < photoWidth; row++)
		{
			photoPixels[index++] = c2;
		}
		for(col = 1; col < photoHeight; col++ )
		{
			System.arraycopy(photoPixels, 0, photoPixels, index, photoWidth);
			index += photoWidth;
		}
		
		index = 0;
		for(row = 0; row < renderWidth; row++)
		{
			renderPixels[index++] = c1;
		}
		for(col = 1; col < renderHeight; col++)
		{
			System.arraycopy(renderPixels, 0, renderPixels, index, renderWidth);
			index += renderWidth;
		}
		
	}	
	
	// produce value of photePixels from renderPixels
	void producePhotoPixels()
	{
		int row, col;
		int index1, old_index1;
		int index2;
		int index2_1,
		    index2_2,
			index2_3,
			index2_4;
		
		if ( needAntialias )
		{
			index2_1 = 0;
			index2_2 = 1;
			index2_3 = renderWidth;
			index2_4 = renderWidth + 1;
			old_index1 = offsetY * photoWidth + offsetX;
			for( row = 0; row < realHeight; row ++ )
			{	
				index1   = old_index1;		  
				for( col = 0; col < realWidth; col ++ )
				{
					photoPixels[index1 ++] = ( ( renderPixels[index2_1 ++] & 0xFCFCFC ) >> 2 )
					                       + ( ( renderPixels[index2_2 ++] & 0xFCFCFC ) >> 2 )
										   + ( ( renderPixels[index2_3 ++] & 0xFCFCFC ) >> 2 )
										   + ( ( renderPixels[index2_4 ++] & 0xFCFCFC ) >> 2 );
					index2_1 ++;
					index2_2 ++;
					index2_3 ++;
					index2_4 ++;
				}
				index2_1 += renderWidth;
				index2_2 += renderWidth;
				index2_3 += renderWidth;
				index2_4 += renderWidth;
				old_index1   += photoWidth;
			}
		}
		else
		{
			/*
			old_index1 = offsetY * photoWidth + offsetX;
			index2 = 0;
			for( row = 0; row < realHeight; row ++ )
			{	
				index1 = old_index1;		  
				for( col = 0; col < realWidth; col ++ )
				{
					photoPixels[index1 ++] = renderPixels[index2 ++];
				}
				old_index1 += photoWidth;
			}
			*/
			index1 = offsetY * photoWidth + offsetX;
			index2 = 0;
			for( row = 0; row < renderHeight; row ++ )
			{
				System.arraycopy(renderPixels, index2, photoPixels, index1, renderWidth);   
				index1 += photoWidth;
				index2 += renderWidth;
			}
		}
		
	}
	
	// cal fps
	void calRenderFPS()
	{
		if ( frameNumber == 0 )
		{
			firstFrameTime = System.currentTimeMillis();
		}
		frameNumber ++;
		if ( frameNumber == staticsFrames )
		{
			lastFrameTime = System.currentTimeMillis();
			renderFPS = (float)(1000 * staticsFrames) / (float)(lastFrameTime - firstFrameTime);
			frameNumber = 0;
		}
	}
	
}
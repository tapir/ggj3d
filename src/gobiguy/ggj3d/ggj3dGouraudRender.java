package gobiguy.ggj3d;

/**
 * ggj3dScene, not only is a container, a scene has
 * environment
 */

import java.util.*;

public class ggj3dGouraudRender extends ggj3dZBufferRender
{

	void renderTriangle(ggj3dTriangle triangle)
	{
		object = triangle.getObject();
		if( object == null ) return;
		material = object.getMaterial();
		if( material == null ) return;
		texture = object.getTexture();
		if( texture == null ) return;
		tW = texture.getWidth();
		tH = texture.getHeight();
		texturePixels = texture.getPixels();
		
		
		p1 = triangle.p1;
		p2 = triangle.p2;
		p3 = triangle.p3;
			
		// sort vertex
		if (p1.y > p2.y) { p = p1; p1 = p2; p2 = p; }
		if (p2.y > p3.y) { p = p2; p2 = p3; p3 = p; }
		if (p1.y > p2.y) { p = p1; p1 = p2; p2 = p; }
			
		if (p1.y >= realHeight) return;
		if (p3.y < 0) return;
		if (p1.y == p3.y) return;
		
		// get vertex 4 
		x1 = p1.x << 8; y1 = p1.y;
		x2 = p2.x << 8; y2 = p2.y;
		x3 = p3.x << 8; y3 = p3.y;
		
		x4 = x1 + ( x3 - x1 ) * ( y2 - y1 ) / ( y3 - y1 );
		x1 <<= 8; x2 <<= 8; x3 <<= 8; x4 <<= 8;

		dx = x4 - x2 >> 16;
		if(dx == 0) return;
		
		z1 = p1.z; 
		z2 = p2.z; 
		z3 = p3.z;
		
		/*
		nx1=(int)(p1.normal.x * 65536f); 
		nx2=(int)(p2.normal.x * 65536f);
		nx3=(int)(p3.normal.x * 65536f);
		
		ny1=(int)(p1.normal.y * 65536f); 
		ny2=(int)(p2.normal.y * 65536f);
		ny3=(int)(p3.normal.y * 65536f);
		
		nz1=(int)(p1.normal.z * 65536f); 
		nz2=(int)(p2.normal.z * 65536f);
		nz3=(int)(p3.normal.z * 65536f);
		*/
	//	nx1 = p1.normal.x; 
	//	nx2 = p2.normal.x;
	//	nx3 = p3.normal.x;
		
	//	ny1 = p1.normal.y; 
	//	ny2 = p2.normal.y;
	//	ny3 = p3.normal.y;
		
	//	nz1 = p1.normal.z; 
	//	nz2 = p2.normal.z;
	//	nz3 = p3.normal.z;
		
		tx1 = p1.tx << 8; 
		tx2 = p2.tx << 8; 
		tx3 = p3.tx << 8;
		
		ty1 = p1.ty << 8; 
		ty2 = p2.ty << 8; 
		ty3 = p3.ty << 8;
		
		calLightDensity(p1);
		r1 = r << 8;
		g1 = g << 8;
		b1 = b << 8;
		
		calLightDensity(p2);
		r2 = r << 8;
		g2 = g << 8;
		b2 = b << 8;
		
		calLightDensity(p3);
		r3 = r << 8;
		g3 = g << 8;
		b3 = b << 8;
		
		
		ratio = ((y2 - y1) << 8) / (y3 - y1);		
		z4  = z1  + (( z3 - z1 ) >> 8) * ratio;
		r4  = r1  + (( r3 - r1 ) >> 8) * ratio;
		g4  = g1  + (( g3 - g1 ) >> 8) * ratio;
		b4  = b1  + (( b3 - b1 ) >> 8) * ratio;
		tx4 = tx1 + ((tx3 - tx1) >> 8) * ratio;
		ty4 = ty1 + ((ty3 - ty1) >> 8) * ratio;
		
	//	ratiof = (float)(y2 - y1) / (float)(y3 - y1);
	//	nx4 = nx1 + (nx3 - nx1) * ratiof;
	//	ny4 = ny1 + (ny3 - ny1) * ratiof;
	//	nz4 = nz1 + (nz3 - nz1) * ratiof;

		dz  = ( z4 - z2 ) / dx;
	//	dnx = (nx4 - nx2) / dx;
	//	dny = (ny4 - ny2) / dx;
	//	dnz = (nz4 - nz2) / dx;
		dtx = (tx4 - tx2) / dx;
		dty = (ty4 - ty2) / dx;
		dr  = ( r4 - r2 ) / dx;
		dg  = ( g4 - g2 ) / dx;
		db  = ( b4 - b2 ) / dx;
	//	dtInd = dty * tW + dtx;
		
		
		if (dx < 0)
		{
			tmp = x4; x4 = x2; x2 = tmp;
			z2 = z4;
			r2 = r4;
			g2 = g4;
			b2 = b4;
		//	nx2 = nx4;
		//	ny2 = ny4;
		//	nz2 = nz4;
			tx2 = tx4;
			ty2 = ty4;
		}
		
		if (y2 >= 0)
		{
			dy = y2 - y1;
			if (dy != 0)
			{
				dxL = (x2 - x1) / dy;
				dxR = (x4 - x1) / dy;
				dzL = (z2 - z1) / dy;
				drL = (r2 - r1) / dy;
				dgL = (g2 - g1) / dy;
				dbL = (b2 - b1) / dy;
			//	dnxL = (nx2 - nx1) / (float)dy;
			//	dnyL = (ny2 - ny1) / (float)dy;
			//	dnzL = (nz2 - nz1) / (float)dy;
				dtxL = (tx2 - tx1) / dy;
				dtyL = (ty2 - ty1) / dy;
		//		dtIndL = dtyL * tW + dtxL;
			}
			
			xL = x1;
			xR = x1;
			zL = z1;
			rL = r1;
			gL = g1;
			bL = b1;
		//	nxL = nx1;
		//	nyL = ny1;
		//	nzL = nz1;
			txL = tx1;
			tyL = ty1;
		//	tIndL = tyL * tW + txL;
			
			if ( y1 < 0 )
			{
				xL -= y1 * dxL;
				xR -= y1 * dxR;
				zL -= y1 * dzL;
				rL -= y1 * drL;
				gL -= y1 * dgL;
				bL -= y1 * dbL;
			//	nxL -= y1 * dnxL;
			//	nyL -= y1 * dnyL;
			//	nzL -= y1 * dnzL;
				txL -= y1 * dtxL;
				tyL -= y1 * dtyL;
			//	tIndL -= y1 * dtIndL;
				y1 = 0;
			}
			
			if( y2 > realHeight ) y2 = realHeight;

			offset = (offsetY + y1) * photoWidth + offsetX; 
			
			for(y = y1; y < y2; y++)
			{
				renderLine();
			}
		}
		
		if (y2 < realHeight)
		{
			dy = y3 - y2;
			if (dy != 0)
			{
				dxL = (x3 - x2) / dy;
				dxR = (x3 - x4) / dy;
				dzL = (z3 - z2) / dy;
				drL = (r3 - r2) / dy;
				dgL = (g3 - g2) / dy;
				dbL = (b3 - b2) / dy;
			//	dnxL = (nx3 - nx2) / (float)dy;
			//	dnyL = (ny3 - ny2) / (float)dy;
			//	dnzL = (nz3 - nz2) / (float)dy;
				dtxL = (tx3 - tx2) / dy;
				dtyL = (ty3 - ty2) / dy;
			//	dtIndL = dtyL * tW + dtxL;
			}
			
			xL = x2;
			xR = x4;
			zL = z2;
			rL = r2;
			gL = g2;
			bL = b2;
		//	nxL = nx2;
		//	nyL = ny2;
		//	nzL = nz2;
			txL = tx2;
			tyL = ty2;
		//	tIndL = tyL * tW + txL;
			
			if ( y2 < 0 )
			{
				xL -= y2 * dxL;
				xR -= y2 * dxR;
				zL -= y2 * dzL;
				rL -= y2 * drL;
				gL -= y2 * dgL;
				bL -= y2 * dbL;
			//	nxL -= y2 * dnxL;
			//	nyL -= y2 * dnyL;
			//	nzL -= y2 * dnzL;
				txL -= y2 * dtxL;
				tyL -= y2 * dtyL;
			//	tIndL -= y2 * dtIndL;
				y2 = 0;
			}
			
			if( y3 > realHeight ) y3 = realHeight;

			offset = (offsetY + y2) * photoWidth + offsetX; 
			
			for(y = y2; y < y3; y++)
			{
				renderLine();
			}
		}
	}
	
	public void renderLine()
	{
		xStart = xL >> 16;
		xEnd = xR >> 16;
		z = zL;
		r = rL;
		g = gL;
		b = bL;
		tx = txL;
		ty = tyL;
		//tInd = tIndL;
		
		if ( xStart < 0 )
		{
			z -= xStart * dz;
			r -= xStart * dr;
			g -= xStart * dg;
			b -= xStart * db;
			tx -= xStart * dtx;
			ty -= xStart * dty;
			//tInd -= xStart * dtInd;
			xStart = 0;
		}
		if( xEnd > realWidth ) xEnd = realWidth;
		
		index = offset + xStart;
		for( x = xStart; x < xEnd; x++)
		{
			try
			{
				tmp = (ty >> 8) * tW + (tx >> 8);
				tmp = tmp < 0 ? 0 : tmp;
				tmp = tmp > (texturePixels.length - 1) ? (texturePixels.length - 1) : tmp;
				color = texturePixels[tmp];
				color = 0xFF000000
				      | ((((((color >> 16) & 0xff) * r) >> 16) & 0xFF) << 16)
				   	  | ((((((color >>  8) & 0xff) * g) >> 16) & 0xFF) <<  8)
					  | ((((((color      ) & 0xff) * b) >> 16) & 0xFF)      );
				if ( z < zBuffer[index] )
				{
					photoPixels[index] = color; 
					zBuffer[index] = z;
				}
			}
			catch(Exception e)
			{
			}
		
			z += dz;
			r += dr;
			g += dg;
			b += db;
			tx += dtx;
			ty += dty;
		//	tInd += dtInd;
			
			index++;
		}
	
		xL += dxL;
		xR += dxR;
		zL += dzL;
		rL += drL;
		gL += dgL;
		bL += dbL;
		txL += dtxL;
		tyL += dtyL;
		//tIndL += dtIndL;
		offset += photoWidth;
	}
	
}
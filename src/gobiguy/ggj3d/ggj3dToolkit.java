
package gobiguy.ggj3d;

/**
 * ggj3dObject, contains many triangles and vertices
 * now, the montor is focus-constant monitor
 */

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.net.*;
import java.util.*;

public class ggj3dToolkit
{

	public static Image getImage(ggj3dPhoto photo)
	{
		return Toolkit.getDefaultToolkit().createImage(
			new MemoryImageSource(
				photo.getWidth(), 
				photo.getHeight(), 
				new DirectColorModel(32,0xFF0000,0xFF00,0xFF), 
				photo.getPixels(), 
				0, 
				photo.getWidth()
				)
			);
	}
	
	
	
	private int currentJunkId;
	private int nextJunkOffset;

	private String currentObjectName = null;
	private ggj3dObject currentObject = null;
	private boolean endOfStream = false;
	Hashtable objectTable;

	public Hashtable getObjectsFrom3dsFile(URL url) throws IOException
	{
		objectTable = new Hashtable();
		
		InputStream is = url.openStream();
		BufferedInputStream in=new BufferedInputStream(is);
		try
		{
			readJunkHeader(in);
			if (currentJunkId!=0x4D4D) {System.out.println("Error: This is no valid 3ds file."); return null; }
			while (!endOfStream) readNextJunk(in);
		}
		catch (Throwable ignored)
		{
		}
		
		return objectTable;
	}
	
	private String readString(InputStream in) throws IOException
	{
		String result=new String();
		byte inByte;
		while ((inByte=(byte)in.read())!=0) result+=(char)inByte;
		return result;
	}

	private int readInt(InputStream in) throws IOException
	{
		return in.read()|(in.read()<<8)|(in.read()<<16)|(in.read()<<24);
	}

	private int readShort(InputStream in) throws IOException
	{
		return (in.read()|(in.read()<<8));
	}

	private float readFloat(InputStream in) throws IOException
	{
		return Float.intBitsToFloat(readInt(in));
	}


	private void readJunkHeader(InputStream in) throws IOException
	{
		currentJunkId=readShort(in);
		nextJunkOffset=readInt(in);
		endOfStream=currentJunkId<0;
	}

	private void readNextJunk(InputStream in) throws IOException
	{
		readJunkHeader(in);
		
		if (currentJunkId==0x3D3D) return; // Mesh block
		if (currentJunkId==0x4000) // Object block
		{
			currentObjectName=readString(in);
			return;
		}
		if (currentJunkId==0x4100)  // Triangular polygon object
		{
			currentObject=new ggj3dObject();
			objectTable.put(currentObjectName, currentObject);
			return;
		}
		if (currentJunkId==0x4110) // Vertex list
		{
			readVertexList(in);
			return;
		}
		if (currentJunkId==0x4120) // Point list
		{
			readPointList(in);
			return;
		}
		if (currentJunkId==0x4140) // Mapping coordinates
		{
			readMappingCoordinates(in);
			return;
		}

		skipJunk(in);
	}

	private void skipJunk(InputStream in) throws IOException, OutOfMemoryError
	{
		for (int i=0; (i<nextJunkOffset-6)&&(!endOfStream);i++)
			endOfStream=in.read()<0;
	}

	private void readVertexList(InputStream in) throws IOException
	{
		float x,y,z;
		int vertices=readShort(in);
		for (int i=0; i<vertices; i++)
		{
			x=readFloat(in);
			y=readFloat(in);
			z=readFloat(in);
			currentObject.addVertex( new ggj3dVertex(x,-y,z) );
		}
	}

	private void readPointList(InputStream in) throws IOException
	{
		int v1,v2,v3;
		int triangles=readShort(in);
		for (int i=0; i<triangles; i++)
		{
			v1=readShort(in);
			v2=readShort(in);
			v3=readShort(in);
			readShort(in);
			currentObject.addTriangle(
				new ggj3dTriangle(
				(ggj3dVertex)(currentObject.verticesVector.elementAt(v2)),
				(ggj3dVertex)(currentObject.verticesVector.elementAt(v1)),
				(ggj3dVertex)(currentObject.verticesVector.elementAt(v3))
				)
				);
		}
	}

	private void readMappingCoordinates(InputStream in) throws IOException
	{
		int vertices=readShort(in);
		float f1, f2;
		for (int i=0; i<vertices; i++)
		{
			/*currentObject.vertex(i).u=readFloat(in);*/
			/*currentObject.vertex(i).v=readFloat(in);*/
			f1 = readFloat(in);
			f2 = readFloat(in);
			//System.out.println("setUV : " + f1 + ", " + f2);
			((ggj3dVertex)(currentObject.verticesVector.elementAt(i))).setUV(f1, f2);//readFloat(in), readFloat(in));
		}
	}	

}

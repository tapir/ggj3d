

package gobiguy.ggj3d;


/**
 * light
 */
 
import gobiguy.ggjmath.*;
 

public class ggj3dLight extends ggj3dComponent
{
	// env light
	// dd - light density, rr - red, gg - green, bb - blue
	int color = 0xffffffff; // ddrrggbb
	
	// transformed pos
	ggjVector p = new ggjVector();
	//ggjVector n = new ggjVector();
	
	// temp no density
	
	
	public ggj3dLight(int c)
	{
		super();
		setColor(c);
	}
	
	/**
	 * set scene
	 */
	public void setScene(ggj3dScene s)
	{
		if( scene == s ) return;
		
		// change scene of self and components
		super.setScene(s);
		s.registerLight(this);;
	}
	
	/**
	 * get position
	 */
	public ggjVector getPosition()
	{
		p.set(vertexMatrix.m03, vertexMatrix.m13, vertexMatrix.m23, 1f);
		return p;
	}
	
	
	public void setColor(int c)
	{
		color = c;
	}
	
	public int getColor()
	{
		return color;
	}
}

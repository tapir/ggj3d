
package gobiguy.ggj3d;

/**
 * environment
 */
 


public class ggj3dEnvironment
{
	// env light
	// 0xddrrggbb - light intensity, rr - red, gg - green, bb - blue
	int light = 0xffffffff; // ddrrggbb
	
	// sound....
	
	
	// construction
	public ggj3dEnvironment(int c)
	{
		setLightColor(c);
	}
	
	/*
	// construction
	public ggj3dEnvironment(int i, int c)
	{
		setLightIntensity(i);
		setLightColor(c);;
	}
	
	//
	public void setLightIntensity(int i)
	{
		light |= ( i & 0xff000000 );
	}
	
	//
	public float getLightIntensity()
	{
		return (float)( light >> 24 );
	}
	*/
	
	//
	public void setLightColor(int c)
	{
		light |= ( c & 0xffffff );
	}
	
	//
	public int getLightColor()
	{
		return light & 0xffffff;
	}
}

package gobiguy.ggj3d;

/**
 * ggj3dScene, not only is a container, a scene has
 * environment
 */

import java.util.*;

public class ggj3dWareFrameRender extends ggj3dZBufferRender
{
	int edgeColor = 0;
	
	public ggj3dWareFrameRender(int c)
	{
		edgeColor = c;
	}
	
	
	
	// draw edges
	void renderTriangle(ggj3dTriangle triangle)
	{
		ggj3dVertex p1, p2, p3;
		int x1, x2, y1, y2, x3, y3;

		p1 = triangle.p1;
		p2 = triangle.p2;
		p3 = triangle.p3;
		
		/*
		x1 = (int)(p1.x);
		y1 = (int)(p1.y);
		x2 = (int)(p2.x);
		y2 = (int)(p2.y);
		x3 = (int)(p3.x);
		y3 = (int)(p3.y);
		*/
		x1 = p1.x;
		y1 = p1.y;
		x2 = p2.x;
		y2 = p2.y;
		x3 = p3.x;
		y3 = p3.y;
		drawLine(x1, y1, x2, y2, 0x0);
		drawLine(x2, y2, x3, y3, 0x0);
		drawLine(x3, y3, x1, y1, 0x0);
	}
	
	// draw a line on photoPixels
	void drawLine(int x1, int y1, int x2, int y2, int color)
	{
		// clip
		int code1 = 0;
		int code2 = 0;
		
		if( x1 < 0 ) code1 |= 1;
		if( x1 >= realWidth ) code1 |= 2;
		if( y1 < 0 ) code1 |= 4;
		if( y1 >= realHeight ) code1 |= 8;
		
		if( x2 < 0 ) code2 |= 1;
		if( x2 >= realWidth ) code2 |= 2;
		if( y2 < 0 ) code2 |= 4;
		if( y2 >= realHeight ) code2 |= 8;
		
		if( (code1 & code2)  != 0 ) return;
		
		float dydx;
		int y3, y4;
		
		if ( x2 != x1 )
		{
			dydx = (float)(y2 - y1) / (float)(x2 - x1);
			
			y3 = y1 + (int)((float)(0 - x1) * dydx);
			y4 = y1 + (int)((float)(realWidth - 1 - x1) * dydx);
			
			if ( x1 < x2 )
			{
				if ( x1 < 0 )
				{
					x1 = 0;
					y1 = y3;
				}
				
				if ( x2 >= realWidth )
				{
					x2 = realWidth - 1;
					y2 = y4;
				}
			}
			else
			{
				if ( x2 < 0 )
				{
					x2 = 0;
					y2 = y3;
				}
				
				if ( x1 >= realWidth )
				{
					x1 = realWidth - 1;
					y1 = y4;
				}
			}
		}
		
		code1 = 0;
		code2 = 0;
		
		if( y1 < 0 ) code1 |= 1;
		if( y1 >= realHeight ) code1 |= 2;
		if( y2 < 0 ) code2 |= 1;
		if( y2 >= realHeight ) code2 |= 2;
		
		if( (code1 & code2) != 0 ) return;
		
		float dxdy;
		int x3, x4;
		
		if ( y1 != y2 )
		{
			dxdy = (float)(x2 - x1) / (float)(y2 - y1);
			x3 = x1 + (int)((float)(0 - y1) * dxdy);
			x4 = x1 + (int)((float)( realHeight - 1- y1) * dxdy);
		
			if ( y1 < y2 )
			{
				if ( y1 < 0 )
				{
					y1 = 0;
					x1 = x3;
				}
				
				if ( y2 >= realHeight )
				{
					y2 = realHeight - 1;
					x2 = x4;
				}
			}
			else
			{
				if ( y2 < 0 )
				{
					y2 = 0;
					x2 = x3;
				}
				
				if ( y1 >= realHeight )
				{
					y1 = realHeight - 1;
					x1 = x4;
				}
			}
		}
		
		int dx = Math.abs(x2 - x1);
		int dy = Math.abs(y2 - y1);
		int p;
		int dd2, dd2_dd1;
		int t;
		int di;
		int index;
		
		if ( dx > dy )
		{
			if ( x1 > x2 )
			{
				t = x1; x1 = x2; x2 = t;
				t = y1; y1 = y2; y2 = t;
			}
			
			di = 1;
			if ( y1 < y2 )
			{
				di += photoWidth;
			}
			else
			{
				di -= photoWidth;
			}
			
			index = photoWidth * (offsetY + y1) + offsetX + x1;
			dd2 = dy + dy;
			p = dd2 - dx;
			dd2_dd1 = p - dx;
			
			//if( index > 0 && index < photoPixels.length)
				photoPixels[index] = color;
			while ( x1 < x2 )
			{
				x1++;
				
				if ( p < 0 )
				{
					p += dd2;
					index ++;
				}
				else
				{
					p += dd2_dd1;
					index += di;
				}
				
			//	if( index > 0 && index < photoPixels.length)
				photoPixels[index] = edgeColor;
			}
		}
		else
		{
			if ( y1 > y2 )
			{
				t = x1; x1 = x2; x2 = t;
				t = y1; y1 = y2; y2 = t;
			}
			
			di = photoWidth;
			if ( x1 < x2 )
			{
				di++;
			}
			else
			{
				di--;
			}
			
			index = photoWidth * (offsetY + y1) + offsetX + x1;
			dd2 = dx + dx;
			p = dd2 - dy;
			dd2_dd1 = p - dy;
			
			//if( index > 0 && index < photoPixels.length)
				photoPixels[index] = color;
			while ( y1 < y2 )
			{
				y1++;
				
				if ( p < 0 )
				{
					p += dd2;
					index += photoWidth;
				}
				else
				{
					p += dd2_dd1;
					index += di;
				}
				
				//if( index > 0 && index < photoPixels.length)
				photoPixels[index] = edgeColor;
			}
		}
	}
		
}
package gobiguy.ggjmath;

/**
 * some math functions
 */ 
 
public class ggjMath
// it is better if rename this to ggjMath,
{
	public static final double PI = 3.1415926d;
	public static final double RPD = PI / 180d; // radian per degree
	
	public ggjMath()
	{
	}
	
	//////////////////////////////////////
	// all functions are static
	//////////////////////////////////////
	
	public static double degreeToRadian(double degree)
	// degree -> radian
	{
		return degree * RPD;
	}
}

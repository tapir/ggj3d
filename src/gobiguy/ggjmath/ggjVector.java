package gobiguy.ggjmath;

/**
 * vector(have 4 components) functions
 */ 
 

public class ggjVector
{
	public float x, y, z, w;

	////////////////////////////////////
	// constuctions
	////////////////////////////////////
  	
	public ggjVector()
	//	throws Exception
	{
		set(0f, 0f, 0f, 1.0f);
	}
	
	public ggjVector(float x, float y, float z)
	//	throws Exception
	{
		set(x, y, z, 1.0f);
	}
	
	public ggjVector(ggjVector from, ggjVector to)
	//	throws Exception
	{
		set(to.x - from.x, to.y - from.y, to.z - from.z, 1.0f);
	}
	
	public ggjVector(float x, float y, float z, float w)
	//	throws Exception
	{
	//	if(w == 0f) throw new Exception("error: w=0");
		set(x, y, z, w);
	}
	
	////////////////////////////////////
	// self changing function
	////////////////////////////////////
	
	public void set(float x, float y, float z, float w)
	//	throws Exception
	{
	//	if(w == 0f) throw new Exception("error: w=0");
		this.x = x; this.y = y; this.z = z; this.w = w;
	}
	
	public void set(ggjVector from, ggjVector to)
	//	throws Exception
	{
		set(to.x - from.x, to.y - from.y, to.z - from.z, 1.0f);
	}
	
	public void set(ggjVector v)
	//	throws Exception
	{
	//	if(w == 0f) throw new Exception("error: w=0");
		this.x = v.x; this.y = v.y; this.z = v.z; this.w = v.w;
	}
	
	public void normalize() // useful as a normal
	{
		float len = (float)Math.sqrt(x * x + y * y + z * z);//getLength();
		if( len == 0f ) return;
		float inv = 1f / ( len * w );
		x *= inv;
		y *= inv;
		z *= inv;
		w = 1f;
	}
	
	public float getLength()
	{
		float len;
		len = x * x + y * y + z * z;
		len = (float)Math.sqrt( len );
		len /= w;
		return len;
	}
	
	public void generalize()
	{
		float inv = 1f / w;
		x *= inv;
		y *= inv;
		z *= inv;
		w = 1f;
	}
	
	public void transform(ggjMatrix m)
	{
		m.transform(this);
	}
	
	public void shift(float dx, float dy, float dz)
	{
		x += dx; y += dy; z += dz;
	}
	
	public void shift(ggjVector v) // == add
	{
		x += v.x; y += v.y; z += v.z; //w += v.w;
	}
	
	public void shiftTo(float x, float y, float z)
	{
		set(x, y, z, 1f);
	}
	
	public void add(ggjVector v) // == shift
	{
		x += v.x; y += v.y; z += v.z; //w += v.w;
	}
	
	public void sub(ggjVector v)
	{
		x -= v.x; y -= v.y; z -= v.z; //w -= v.w;
	}
	
	public void scale(float s)
	{
		x *= s; y *= s; z *= s;
	}
	
	public void rotate(float dalpha, float dbeta, float dgamma)
	{
		if (dalpha != 0f)
		{
			rotateX(dalpha);
		}
		if (dbeta != 0f)
		{
			rotateY(dbeta);
		}
		if (dgamma != 0f)
		{
			rotateZ(dgamma);
		}
	}
	
	public void rotateX(float alpha)
	{
		float r = (float)(ggjMath.degreeToRadian(alpha));
		float c = (float)(Math.cos(r));
		float s = (float)(Math.sin(r));
		
		float y1, z1;
		y1 = y * c - z * s;
		z1 = y * s + z * c;
		y = y1; z = z1;
	}

	public void rotateY(float beta)
	{
		float r = (float)(ggjMath.degreeToRadian(beta));
		float c = (float)(Math.cos(r));
		float s = (float)(Math.sin(r));
		
		float x1, z1;
		z1 = z * c - x * s;
		x1 = z * s + x * c;
		z = z1; x = x1;
	}
	
	public void rotateZ(float gamma)
	{
		float r = (float)(ggjMath.degreeToRadian(gamma));
		float c = (float)(Math.cos(r));
		float s = (float)(Math.sin(r));
		
		float x1, y1;
		x1 = x * c - y * s;
		y1 = x * s + y * c;
		x = x1; y = y1;
	}
	
	public float dotProduct(ggjVector v)
	{
		return dotProduct(this, v);
	}

	////////////////////////////////////
	// static function
	////////////////////////////////////

	// crossProduct bettor
	public static void forkProduct(ggjVector u, ggjVector v, ggjVector w)
	{
		w.x = u.y * v.z - v.y * u.z;
		w.y = u.z * v.x - v.z * u.x;
		w.z = u.x * v.y - v.x * u.y;
		w.w = u.w * v.w;
	}
	
	public static ggjVector forkProduct(ggjVector u, ggjVector v)
	{
		ggjVector w = new ggjVector();
		forkProduct(u, v, w);
		return w;
	}
	
	public static float dotProduct(ggjVector u, ggjVector v)
	{
		float dot;
		dot = u.x * v.x + u.y * v.y + u.z * v.z;
		dot /= (u.w * v.w);
		
		return dot;
	}

	public static void transform(ggjMatrix m, ggjVector u, ggjVector v)
	{
		m.transform(u, v);
	}

	public static ggjVector transform(ggjMatrix m, ggjVector u)
	{
		ggjVector v = new ggjVector();
		m.transform(u, v);
		return v;
	}
	
	public void print()
	{
		System.out.println(": x=" + x + ", y=" + y + ",z=" + z + ",w=" + w);
	}
}

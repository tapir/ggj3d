package gobiguy.ggjmath;

/**
 * some matix functions
 */ 
 


public class ggjMatrix
{
	public float m00=1, m01=0, m02=0, m03=0;
	public float m10=0, m11=1, m12=0, m13=0;
	public float m20=0, m21=0, m22=1, m23=0;
	public float m30=0, m31=0, m32=0, m33=1;
	
	
	/////////////////////////////
	// contruction function
	/////////////////////////////
	public ggjMatrix()
	{
	}
	
	public void set(ggjVector u, ggjVector v, ggjVector w)
	// u, v, w should be normalized vector
	{
		m00=u.x; m01=v.x; m02=w.x; m03=0;
		m10=u.y; m11=v.y; m12=w.y; m13=0;
		m20=u.z; m21=v.z; m22=w.z; m23=0;
		m30=0;   m31=0;   m32=0;   m33=1;
	}
	
	public void set(ggjVector u, ggjVector v, ggjVector w, ggjVector pos)
	// u, v, w should be normalized vector, pos should be commom vector
	{
		m00=u.x; m01=v.x; m02=w.x; m03=pos.x;
		m10=u.y; m11=v.y; m12=w.y; m13=pos.y;
		m20=u.z; m21=v.z; m22=w.z; m23=pos.z;
		m30=0;   m31=0;   m32=0;   m33=1;
	}
	
	public void set(ggjMatrix m)
	{
		m00=m.m00; m01=m.m01; m02=m.m02; m03=m.m03;
		m10=m.m10; m11=m.m11; m12=m.m12; m13=m.m13;
		m20=m.m20; m21=m.m21; m22=m.m22; m23=m.m23;
		m30=m.m30; m31=m.m31; m32=m.m32; m33=m.m33;
	}
	
	public void reset()
	{
		m00=1; m01=0; m02=0; m03=0;
		m10=0; m11=1; m12=0; m13=0;
		m20=0; m21=0; m22=1; m23=0;
		m30=0; m31=0; m32=0; m33=1;
	}
	
	///////////////////////////
	// multiply functions
	///////////////////////////
	public void multiply(ggjMatrix m) // this = this * m
	{
		float f0, f1, f2, f3;
		
		// row 1
		f0 = m00 * m.m00 + m01 * m.m10 + m02 * m.m20 + m03 * m.m30;
		f1 = m00 * m.m01 + m01 * m.m11 + m02 * m.m21 + m03 * m.m31;
		f2 = m00 * m.m02 + m01 * m.m12 + m02 * m.m22 + m03 * m.m32;
		f3 = m00 * m.m03 + m01 * m.m13 + m02 * m.m23 + m03 * m.m33;
		m00 = f0; m01 = f1; m02 = f2; m03 = f3;
		// row 2
		f0 = m10 * m.m00 + m11 * m.m10 + m12 * m.m20 + m13 * m.m30;
		f1 = m10 * m.m01 + m11 * m.m11 + m12 * m.m21 + m13 * m.m31;
		f2 = m10 * m.m02 + m11 * m.m12 + m12 * m.m22 + m13 * m.m32;
		f3 = m10 * m.m03 + m11 * m.m13 + m12 * m.m23 + m13 * m.m33;
		m10 = f0; m11 = f1; m12 = f2; m13 = f3;
		// row 3
		f0 = m20 * m.m00 + m21 * m.m10 + m22 * m.m20 + m23 * m.m30;
		f1 = m20 * m.m01 + m21 * m.m11 + m22 * m.m21 + m23 * m.m31;
		f2 = m20 * m.m02 + m21 * m.m12 + m22 * m.m22 + m23 * m.m32;
		f3 = m20 * m.m03 + m21 * m.m13 + m22 * m.m23 + m23 * m.m33;
		m20 = f0; m21 = f1; m22 = f2; m23 = f3;
		// row 4
		f0 = m30 * m.m00 + m31 * m.m10 + m32 * m.m20 + m33 * m.m30;
		f1 = m30 * m.m01 + m31 * m.m11 + m32 * m.m21 + m33 * m.m31;
		f2 = m30 * m.m02 + m31 * m.m12 + m32 * m.m22 + m33 * m.m32;
		f3 = m30 * m.m03 + m31 * m.m13 + m32 * m.m23 + m33 * m.m33;
		m30 = f0; m31 = f1; m32 = f2; m33 = f3;
	}
	
	public void multiplied(ggjMatrix m) // this = m * this
	{
		float f0, f1, f2, f3;
		
		// col 1
		f0 = m.m00 * m00 + m.m01 * m10 + m.m02 * m20 + m.m03 * m30;
		f1 = m.m10 * m00 + m.m11 * m10 + m.m12 * m20 + m.m13 * m30;
		f2 = m.m20 * m00 + m.m21 * m10 + m.m22 * m20 + m.m23 * m30;
		f3 = m.m30 * m00 + m.m31 * m10 + m.m32 * m20 + m.m33 * m30;
		m00 = f0; m10 = f1; m20 = f2; m30 = f3;
		// col 2
		f0 = m.m00 * m01 + m.m01 * m11 + m.m02 * m21 + m.m03 * m31;
		f1 = m.m10 * m01 + m.m11 * m11 + m.m12 * m21 + m.m13 * m31;
		f2 = m.m20 * m01 + m.m21 * m11 + m.m22 * m21 + m.m23 * m31;
		f3 = m.m30 * m01 + m.m31 * m11 + m.m32 * m21 + m.m33 * m31;
		m01 = f0; m11 = f1; m21 = f2; m31 = f3;
		// col 3
		f0 = m.m00 * m02 + m.m01 * m12 + m.m02 * m22 + m.m03 * m32;
		f1 = m.m10 * m02 + m.m11 * m12 + m.m12 * m22 + m.m13 * m32;
		f2 = m.m20 * m02 + m.m21 * m12 + m.m22 * m22 + m.m23 * m32;
		f3 = m.m30 * m02 + m.m31 * m12 + m.m32 * m22 + m.m33 * m32;
		m02 = f0; m12 = f1; m22 = f2; m32 = f3;
		// col 4
		f0 = m.m00 * m03 + m.m01 * m13 + m.m02 * m23 + m.m03 * m33;
		f1 = m.m10 * m03 + m.m11 * m13 + m.m12 * m23 + m.m13 * m33;
		f2 = m.m20 * m03 + m.m21 * m13 + m.m22 * m23 + m.m23 * m33;
		f3 = m.m30 * m03 + m.m31 * m13 + m.m32 * m23 + m.m33 * m33;
		m03 = f0; m13 = f1; m23 = f2; m33 = f3;
	}
	
	public void multiplyInv(ggjMatrix m)
	{
		m.inverse();
		multiply(m);
		m.inverse();
	}
	
	public void multipliedInv(ggjMatrix m)
	{
		m.inverse();
		multiplied(m);
		m.inverse();
	}
	
	public void invMultiply(ggjMatrix m)
	{
		inverse();
		multiply(m);
	}
	
	public void invMultiplied(ggjMatrix m)
	{
		inverse();
		multiplied(m);
	}
	
	/*
	public void multiplyTran(ggjMatrix m)
	{
		m.transpose(); // danger if more than one renders running at one time,
		multiply(m);
		m.transpose();
	}
	
	public void tranMultiply(ggjMatrix m)
	{
		transpose();
		multiply(m);
	}
	*/
	
	//////////////////////////////
	// self transform functions
	//////////////////////////////
	public void shift(float dx, float dy, float dz)
	{
		m03 += dx;
		m13 += dy;
		m23 += dz;
	}
	
	public void rotate(float dalpha, float dbeta, float dgamma)
	{
		if (dalpha != 0f)
		{
			rotateX(dalpha);
		}
		if (dbeta != 0f)
		{
			rotateY(dbeta);
		}
		if (dgamma != 0f)
		{
			rotateZ(dgamma);
		}
	}
	
	public void rotateX(float alpha)
	{
		float r = (float)(ggjMath.degreeToRadian(alpha));
		float c = (float)(Math.cos(r));
		float s = (float)(Math.sin(r));
		
		float x, y, z, w;
		x = m10 * c - m20 * s;
		y = m11 * c - m21 * s;
		z = m12 * c - m22 * s;
		w = m13 * c - m23 * s;
		
		m20 = m10 * s + m20 * c;
		m21 = m11 * s + m21 * c;
		m22 = m12 * s + m22 * c;
		m23 = m13 * s + m23 * c;
		
		m10 = x;
		m11 = y;
		m12 = z;
		m13 = w;
	}

	public void rotateY(float beta)
	{
		float r = (float)(ggjMath.degreeToRadian(beta));
		float c = (float)(Math.cos(r));
		float s = (float)(Math.sin(r));
		
		float x, y, z, w;
		x = m20 * c - m00 * s;
		y = m21 * c - m01 * s;
		z = m22 * c - m02 * s;
		w = m23 * c - m03 * s;
		
		m00 = m20 * s + m00 * c;
		m01 = m21 * s + m01 * c;
		m02 = m22 * s + m02 * c;
		m03 = m23 * s + m03 * c;
		
		m20 = x;
		m21 = y;
		m22 = z;
		m23 = w;
	}
	
	public void rotateZ(float gamma)
	{
		float r = (float)(ggjMath.degreeToRadian(gamma));
		float c = (float)(Math.cos(r));
		float s = (float)(Math.sin(r));
		
		float x, y, z, w;
		x = m00 * c - m10 * s;
		y = m01 * c - m11 * s;
		z = m02 * c - m12 * s;
		w = m03 * c - m13 * s;
		
		m10 = m00 * s + m10 * c;
		m11 = m01 * s + m11 * c;
		m12 = m02 * s + m12 * c;
		m13 = m03 * s + m13 * c;
		
		m00 = x;
		m01 = y;
		m02 = z;
		m03 = w;
	}
	
	public void transpose()
	{
		float tmp;
		tmp = m01; m01 = m10; m10 = tmp;
		tmp = m02; m02 = m20; m20 = tmp;
		tmp = m03; m03 = m30; m30 = tmp;
		tmp = m12; m12 = m21; m21 = tmp;
		tmp = m13; m13 = m31; m31 = tmp;
		tmp = m23; m23 = m32; m32 = tmp;
	}

	public void inverse()
	// not a universal method
	{
		float tmp;
		tmp = m01; m01 = m10; m10 = tmp;
		tmp = m02; m02 = m20; m20 = tmp;
		tmp = m12; m12 = m21; m21 = tmp;
		
		float t0, t1, t2;
		t0 = -m03;
		t1 = -m13;
		t2 = -m23;
		
		m03 = m00 * t0 + m01 * t1 + m02 * t2;
		m13 = m10 * t0 + m11 * t1 + m12 * t2;
		m23 = m20 * t0 + m21 * t1 + m22 * t2;
		
		//System.out.println("inv");
		//print();
	}
	
	////////////////////////////
	//  transform vector function
	/////////////////////////////
	public void transform(ggjVector u, ggjVector v)
	{
		float x = u.x, y = u.y, z= u.z, w = u.w;

		v.x = m00 * x + m01 * y + m02 * z + m03 * w;
		v.y = m10 * x + m11 * y + m12 * z + m13 * w;
		v.z = m20 * x + m21 * y + m22 * z + m23 * w;
		v.w = m30 * x + m31 * y + m32 * z + m33 * w;
	}
	
	public void transform(ggjVector u)
	{
		float x = u.x, y = u.y, z= u.z, w = u.w;
		
		u.x = m00 * x + m01 * y + m02 * z + m03 * w;
		u.y = m10 * x + m11 * y + m12 * z + m13 * w;
		u.z = m20 * x + m21 * y + m22 * z + m23 * w;
		u.w = m30 * x + m31 * y + m32 * z + m33 * w;
	}
	
	public void print()
	{
		String str = "";
		
		str += m00 + "\t" + m01 + "\t" + m02 + "\t" + m03 + "\n";
		str += m10 + "\t" + m11 + "\t" + m12 + "\t" + m13 + "\n";
		str += m20 + "\t" + m21 + "\t" + m22 + "\t" + m23 + "\n";
		str += m30 + "\t" + m31 + "\t" + m32 + "\t" + m33 + "\n";
		
		System.out.println(str);
	}
}

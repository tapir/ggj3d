package demo;

import gobiguy.ggj3d.*;
import gobiguy.ggjmath.*;

import java.awt.*;
import java.applet.*;
import java.util.*;
import java.net.*;
import java.awt.event.*;

public class ggj3dTest5 extends Applet implements Runnable, KeyListener
{
	/////////////////////// 
	Thread thread;
	Image memoryImage;
	long timeStep = 50L;
	
	///////////////// model used ///////////////////////////
	// scene
	ggj3dScene scene = new ggj3dScene();
	
	// env
	ggj3dEnvironment sunnyEnvironment = new ggj3dEnvironment(0xffffffff);
	
	// lights
	ggj3dLight whiteLight = new ggj3dLight(0xffffffff);
	ggj3dLight carLight1 = new ggj3dLight(0xff770000);
	ggj3dLight carLight2 = new ggj3dLight(0xff770000);
	
	// monitors
	ggj3dMonitor mainMonitor = new ggj3dMonitor();
	ggj3dMonitor auxMonitor1 = new ggj3dMonitor();
	ggj3dMonitor auxMonitor2 = new ggj3dMonitor();
	
	// photos
	ggj3dPhoto mainPhoto = new ggj3dPhoto(300, 300);
	ggj3dPhoto auxPhoto1 = new ggj3dPhoto(300, 300);
	ggj3dPhoto auxPhoto2 = new ggj3dPhoto(300, 300);
	
	// materials
	ggj3dMaterial woodMaterial = new ggj3dMaterial(0.0f, 0.4f, 0.2f, 4f);
	ggj3dMaterial ironMaterial = new ggj3dMaterial(0.0f, 0.4f, 0.2f, 4f);

	// textures
	ggj3dTexture groundTexture = new ggj3dTexture(0xffffff);
	ggj3dTexture treeTexture = new ggj3dTexture(0x777777);
	ggj3dTexture hillTexture = new ggj3dTexture(0xffffff);
	ggj3dTexture carTexture = new ggj3dTexture(0xffffff);

	// scene renders
	ggj3dSceneRender wireFrameRender = new ggj3dWareFrameRender(0xff0000);
	ggj3dSceneRender mainRender = new ggj3dGouraudRender2();
	ggj3dSceneRender auxRender1 = new ggj3dGouraudRender();
	ggj3dSceneRender auxRender2 = new ggj3dGouraudRender();

	// tool
	ggj3dToolkit toolkit = new ggj3dToolkit();
	
	// components
	ggj3dContainer car = new ggj3dContainer();
	float carSpeed = 0f;


	///////////////////////////// main functions /////////////
	// init
	public void init()
	{
		// setup monitors
		ggjVector p = new ggjVector(0, 0, 0);
		ggjVector u = new ggjVector(1, 0, 0);
		ggjVector v = new ggjVector(0, 1, 0);
		ggjVector w = new ggjVector(0, 0, -1);
		
		mainMonitor.setAxises(u, v, w);
		mainMonitor.setPosition(p);
		mainMonitor.setCurrentPhoto(mainPhoto);
		mainMonitor.setSceneRender(mainRender);
		mainMonitor.setLenFOV(90, 88);
		
		auxMonitor1.setAxises(u, v, w);
		auxMonitor1.setPosition(p);
		auxMonitor1.setCurrentPhoto(auxPhoto1);
		auxMonitor1.setSceneRender(auxRender1);
		
		auxMonitor2.setAxises(u, v, w);
		auxMonitor2.setPosition(p);
		auxMonitor2.setCurrentPhoto(auxPhoto2);
		auxMonitor2.setSceneRender(auxRender2);
		
		// load texture
		treeTexture = new ggj3dTexture(getDocumentBase(), "../package/demo/pictures/stone4.jpg");
		
		// setup car
		car.add(mainMonitor); //mainMonitor.shift(0f, 5f, 0f);
		car.add(carLight1); carLight1.shift(-2f, 1f, 0f);
		car.add(carLight2); carLight2.shift(2f, 1f, 0f);
		
		// setup scene
		scene.setEnvironment(sunnyEnvironment);
		scene.add(whiteLight); whiteLight.shift(100f, 100f, 300f);
		scene.add(car); car.shift(0f, 0f, 5f);
		
		ggj3dObject object;
		object = getObject("../package/demo/models/spere4.3ds", treeTexture, woodMaterial);
		if ( object != null )
		{
			object.shift(0f, 0f, 2f);
			scene.add(object);
		}
		/*
		object = getObject("", treeTexture, woodMaterial);
		object.shift(0f, 0f, 2f);
		object = getObject("", treeTexture, woodMaterial);
		object.shift(-4f, 0f, 2f);
		scene.add(object);
		object = getObject("", treeTexture, woodMaterial);
		object.shift(0f, 0f, -2f);
		scene.add(object);
		object = getObject("", treeTexture, woodMaterial);
		object.shift(-4f, 0f, -2f);
		scene.add(object);
		*/
		
		
		///////////////////////////////////////
		addKeyListener(this);
	}
	
	// paint
	public void paint(Graphics g)
	{
		mainMonitor.shoot(scene);
		
		Dimension size = getSize();
		int left = size.width - mainPhoto.getWidth() >> 1;
		int top = size.height - mainPhoto.getHeight() >> 1;
		memoryImage = ggj3dToolkit.getImage(mainPhoto);
		
		g.drawImage(memoryImage, left, top, this);
		
	}
	
	// update scene
	void updateScene()
	{
		car.shiftSelf(0f, 0f, -carSpeed);
	}
	
	//
	ggj3dObject getObject(String file, ggj3dTexture t, ggj3dMaterial m)
	{
		ggj3dObject o;
		
		if ( file == "" )
		{
			o = new ObjectB();
			o.setTexture(t);
			o.setMaterial(m);
		}
		else
		{
			Hashtable table;
			try
			{
				table = toolkit.getObjectsFrom3dsFile(new URL(getDocumentBase(), file));
				if( table == null )
					o = null;
				else
					if (table.size() == 0)
				 		o = null;
					else
					{
						Enumeration enum = table.elements();
						o = (ggj3dObject)(enum.nextElement());
						o.setTexture(t);
						o.setMaterial(m);
					}
						
			}
			catch(Exception e)
			{
				o = null;
			}
			
				
		}
		
		return o;
	}
	
	////////////////////////////// event ////////////////
	
	//
	public void keyPressed(KeyEvent ke)
	{
		int code = ke.getKeyCode();
		switch ( code )
		{
		case KeyEvent.VK_UP:
			carSpeed += 0.05f;
			car.shiftSelf(0f, 0f, -0.3f);
			break;
		case KeyEvent.VK_DOWN:
			carSpeed -= 0.05f;
			car.shiftSelf(0f, 0f, 0.3f);
			break;
		case KeyEvent.VK_LEFT:
			car.rotateSelf(0f, 1f, 0f);
			break;
		case KeyEvent.VK_RIGHT:
			car.rotateSelf(0f, -1f, 0f);
			break;
		default:
		}
	}
	
	public void keyReleased(KeyEvent ke)
	{
	}
	
	public void keyTyped(KeyEvent ke)
	{
	}
	
	
	//////////////////////////// other /////////////////
	public void start()
	{
		if (thread == null)
		{
			thread = new Thread(this);
			thread.start();
		}
	}
	
	public void stop()
	{
		if (thread != null)
		{
			thread = null;
		}
	}

	public void run()
	{
		long time;
		while( thread != null)
		{
			repaint();
			try
			{
				time = System.currentTimeMillis();
				updateScene();
				thread.sleep( Math.max(0, time + timeStep - System.currentTimeMillis()) );
			}
			catch (InterruptedException e)
			{
				System.out.println("InterruptedException");
			}
		}
	}

	public void update(Graphics g)
	{
		paint(g);
	}
	
	public boolean imageUpdate(Image image, int i, int j, int k, int l, int i1)
	{
	    return true;
	}

}

package demo;

import gobiguy.ggj3d.*;
import gobiguy.ggjmath.*;

import java.awt.*;
import java.applet.*;
import java.util.*;
import java.net.*;

public class ggj3dTest10 extends Applet implements Runnable
{
	Thread thread;
	
	Image image;
	
	Image memoryImage;
	
	ggj3dScene scene1 = new ggj3dScene();
	ggj3dObject head;
	ggj3dObject[] objects = new ggj3dObject[10];
	
	ggj3dMaterial material1 = new ggj3dMaterial(0.2f, 0.8f, 0.2f, 4f);
	ggj3dTexture texture1 = new ggj3dTexture(0xeeaa88);
	ggj3dTexture texture2;
	ggj3dLight light1 = new ggj3dLight(0xff0000ff);
	ggj3dLight light2 = new ggj3dLight(0xffff0000);
	ggj3dLight light3 = new ggj3dLight(0xffffffff);
	ggj3dLight light4 = new ggj3dLight(0xffff00ff);
	ggj3dLight light5 = new ggj3dLight(0xffffff00);
	ggj3dLight light6 = new ggj3dLight(0xff00ffff);
	ggj3dLight light7 = new ggj3dLight(0xffffffff);
	ggj3dLight light8 = new ggj3dLight(0xffffffff);
	ggj3dLight light9 = new ggj3dLight(0xffffffff);
	ggj3dEnvironment environment1 = new ggj3dEnvironment(0xffffffff);
	
	ggjVector p = new ggjVector(0, 0, 550f);
	ggjVector u = new ggjVector(1, 0, 0);
	ggjVector v = new ggjVector(0, 1, 0);
	ggjVector w = new ggjVector(0, 0, -1);
	ggj3dMonitor monitor1 = new ggj3dMonitor();
	ggj3dMonitor monitor2 = new ggj3dMonitor();
	ggj3dPhoto photo1 = new ggj3dPhoto(500, 500);
	ggj3dPhoto photo2 = new ggj3dPhoto(500, 500);
	ggj3dSceneRender render1 = new ggj3dWareFrameRender2(0xff);
	ggj3dSceneRender render2 = new ggj3dGouraudRender2();
	ggj3dSceneRender render3 = new ggj3dPhoneRender();
	ggj3dSceneRender render4 = new ggj3dGouraudRender();

	ggj3dToolkit toolkit = new ggj3dToolkit();
	
	ggj3dObject objectB = new ObjectB();
	

	public void init()
	{
		texture2 = new ggj3dTexture(getDocumentBase(), "../package/demo/pictures/earthmap.jpg");
		scene1.setEnvironment(environment1);
		scene1.setBgColor(0xff000000);
		
	//	scene1.add(light1); light1.shift(0f, 6f, 0f);
	//	scene1.add(light2); light2.shift(0f, 8f, 0f);
		scene1.add(light3); light3.shift(100f, 100f, 100f);
	//	scene1.add(light4); light4.shift(-6f, 0f, 0f);
	//	scene1.add(light5); light5.shift(8f, 0f, 0f);
		scene1.add(light6); light6.shift(-100f, -100f, 80f);
	//	scene1.add(light7); light7.shift(-6f, 0f, 0f);
	//	scene1.add(light8); light8.shift(8f, 0f, 0f);
	//	scene1.add(light9); light9.shift(0f, 0f, 8f);
		
	//	scene1.add(objectB); 
	//		objectB.shift(0, 0, 3);		
	//		objectB.setTexture(texture2);
	//		objectB.setMaterial(material1);
	//	scene1.remove(objectB);
	
		scene1.setName("scene 1");
		
		monitor1.setPosition(p);
		monitor1.setAxises(u, v, w);  
		monitor1.rotate(90, 0, 0);
		monitor1.setLenFOV(30, 30);


		monitor1.setSceneRender(render2);
		render2.setAntialias(true);
		monitor1.setCurrentPhoto(photo1);

		
		monitor2.setPosition(p);
		monitor2.setAxises(u, v, w);  
		monitor2.rotate(0, 90, 0);
		monitor2.setLenFOV(30, 30);
		
		try
		{

			Hashtable table = toolkit.getObjectsFrom3dsFile(new URL(getDocumentBase(), "../package/demo/models/teapot.3DS"));
			Enumeration enum = table.elements();
			ggj3dObject o;
			int index = 0;
			//while( enum.hasMoreElements() )
			for(int i = 0; i < 1; i++)
			{
				o = (ggj3dObject)(enum.nextElement());
				o.setTexture(texture1);
				o.setMaterial(material1);
			//	o.shift(0, 2, 0);
				//o.rotate(-90, 0, 0);
				head = o;
				scene1.add( head );
			//	if(index > 3) continue;
			//	objects[index] = o;
				objects[0] = o;
				index++;
			}
			//objects[0].shift(1f, 0f, 0f);
			//objects[1].rotatePosition(20f, 10f, 0f);
			//objects[2].rotate(-20f, -20f, 0f);
						
		}
		catch(Exception exception)
		{
		    System.out.println(exception + "");
		}

	}
	
	public void start()
	{
		if (thread == null)
		{
			thread = new Thread(this);
			thread.start();
		}
	}
	
	public void stop()
	{
		if (thread != null)
		{
			thread = null;
		}
	}

	public void run()
	{
		long start_time, dt;

		while( thread != null)
		{
			start_time = System.currentTimeMillis();

			repaint();
			try
			{
				//objects[0].rotate(0f, 1f, 0f);
				//objects[1].rotatePosition(1f, 0f, 0f);
			//	objects[0].rotateSelf(0f, 1f, 0f);
			//	objects[1].rotateSelf(1f, 0f, 0f);
			//	objects[2].rotate(0f, 1f, 0f);
			//	light3.rotate(1f, 0f, 0f);
			//	monitor1.rotate(0f, 1f, 0f);
			//	objectB.rotatePosition(0f, 1f, 0f);
			//	monitor2.shift(0f, -0.05f, -0.00f);
			
				head.rotateSelf(0f, 0f, 1f);
			
			
				//
				dt = System.currentTimeMillis() - start_time;
			
			//	System.out.println(" start_time: " + start_time + "ms");
			//	System.out.println(" now_time: " + System.currentTimeMillis() + "ms");
			//	System.out.println(" render time: " + dt + "ms");
			
				thread.sleep( Math.max( 0L,  100L - dt) );
				
			}
			catch (InterruptedException e)
			{
			//	System.out.println("InterruptedException");
			}
		}
	}

	public void update(Graphics g)
	{
		paint(g);
	}
	
	public void paint(Graphics g)
	{
		float fps;
		
		monitor1.shoot(scene1);		
		memoryImage = ggj3dToolkit.getImage(photo1);
		
		Dimension sz1 = getSize();
	//	int left = (sz1.width >> 1) - photo1.getWidth() >> 1;
		int left = (sz1.width) - photo1.getWidth() >> 1;
		int top = sz1.height - photo1.getHeight() >> 1;
		
		g.drawImage(memoryImage, left, top, this);
		
		fps = render2.getRenderFPS();
		g.setColor(Color.white);
		g.drawString("fps: " + fps, 10, 10);
		
		/*
		monitor1.setSceneRender(render1);
		monitor1.setCurrentPhoto(photo2);
		monitor1.shoot(scene1);		
		memoryImage = ggj3dToolkit.getImage(photo2);
		*/
		
		/*
		monitor2.setSceneRender(render2);
		monitor2.setCurrentPhoto(photo2);
		monitor2.shoot(scene1);		
		memoryImage = ggj3dToolkit.getImage(photo2);
		
		left += sz1.width >> 1;
		top = sz1.height - photo2.getHeight() >> 1;
		g.drawImage(memoryImage, left, top, this);
		*/
	}
	
	public boolean imageUpdate(Image image, int i, int j, int k, int l, int i1)
	{
	    return true;
	}

	void print(String head, ggjVector v)
	{
		System.out.println(head + ": x=" + v.x + ", y=" + v.y + ",z=" + v.z + ",w=" + v.w);
	}
}

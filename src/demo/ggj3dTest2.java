package demo;


import gobiguy.ggj3d.*;
import gobiguy.ggjmath.*;

import java.awt.*;
import java.applet.*;
import java.util.*;
import java.net.*;

public class ggj3dTest2 extends Applet implements Runnable
{
	Thread thread;
	
	Image image;
	
	Image memoryImage;
	
	ggj3dScene scene1 = new ggj3dScene();
	ggj3dObject[] objects = new ggj3dObject[4];
	
	ggj3dMaterial material1 = new ggj3dMaterial(0.0f, 0.9f, 0.2f, 4f);
	ggj3dTexture texture1 = new ggj3dTexture(0xff0000);
	ggj3dTexture texture2;
	ggj3dLight light1 = new ggj3dLight(0xffffffff);
	ggj3dLight light2 = new ggj3dLight(0xffffffff);
	ggj3dEnvironment environment1 = new ggj3dEnvironment(0xffffffff);
	
	ggjVector p = new ggjVector(0, 0, 150);
	ggjVector u = new ggjVector(1, 0, 0);
	ggjVector v = new ggjVector(0, 1, 0);
	ggjVector w = new ggjVector(0, 0, -1);
	ggj3dMonitor monitor1 = new ggj3dMonitor();
	ggj3dPhoto photo1 = new ggj3dPhoto(300, 300);
	ggj3dSceneRender render1 = new ggj3dWareFrameRender2(0xff0000);
	ggj3dSceneRender render2 = new ggj3dGouraudRender2();

	ggj3dToolkit toolkit = new ggj3dToolkit();

	public void init()
	{
		texture2 = new ggj3dTexture(getDocumentBase(), "../package/demo/pictures/orange_1.JPG");
		scene1.setEnvironment(environment1);
		scene1.add(light1); light1.shift(0, 3000, 0);
		scene1.add(light2); light2.shift(0, -3000, 0);
		scene1.add(monitor1);
	
		scene1.setName("scene 1");
		
		monitor1.setPosition(p);
		monitor1.setAxises(u, v, w);
		monitor1.rotate(90, 0, 0);
		monitor1.setLenFOV(100, 60);
		monitor1.setSceneRender(render2);
		monitor1.setCurrentPhoto(photo1);
		
		try
		{
			Hashtable table = toolkit.getObjectsFrom3dsFile(new URL(getDocumentBase(), "../package/demo/models/linkable.3ds"));
			Enumeration enum = table.elements();
			ggj3dObject o;
			int index = 0;
			while( enum.hasMoreElements() )
			//for(int i = 0; i < 3; i++)
			{
				o = (ggj3dObject)(enum.nextElement());
				o.setTexture(texture2);
				o.setMaterial(material1);
				scene1.add( o );
				if(index > 3) continue;
				objects[index] = o;
				index++; 
			}
			//objects[0].shift(10f, 20f, 0f);
			//objects[1].rotatePosition(20f, 10f, 0f);
			//objects[2].rotate(-20f, -20f, 0f);
						
		}
		catch(Exception exception)
		{
		    System.out.println(exception + "");
		}
		
	}
	
	public void start()
	{
		if (thread == null)
		{
			thread = new Thread(this);
			thread.start();
		}
	}
	
	public void stop()
	{
		if (thread != null)
		{
			thread = null;
		}
	}

	public void run()
	{
		while( thread != null)
		{
			repaint();
			try
			{
				objects[0].rotateSelf(0f, 0f, -1f);
				objects[1].rotatePosition(1f, 0f, 0f);
				objects[2].rotate(1f, 0f, 0f);
				objects[3].rotate(1f, 0f, 0f);
				light1.rotate(0f, 0f, -1f);
				monitor1.rotate(0f, 0f, 1f);
				//monitor1.shift(0f, -0.1f, 0f);
				thread.sleep(20);
			}
			catch (InterruptedException e)
			{
				System.out.println("InterruptedException");
			}
		}
	}

	public void update(Graphics g)
	{
		paint(g);
	}
	
	public void paint(Graphics g)
	{
		monitor1.shoot(scene1);
		
		ggj3dPhoto photo = monitor1.getCurrentPhoto();
		memoryImage = ggj3dToolkit.getImage(photo);
		
		Dimension sz1 = getSize();
		int left = sz1.width - photo.getWidth() >> 1;
		int top = sz1.height - photo.getHeight() >> 1;
		
		g.drawImage(memoryImage, left, top, this);
		
		float fps;
		fps = render2.getRenderFPS();
		g.setColor(Color.white);
		g.drawString("fps: " + fps, 10, 10);
	}
	
	public boolean imageUpdate(Image image, int i, int j, int k, int l, int i1)
	{
	    return true;
	}

	void print(String head, ggjVector v)
	{
		System.out.println(head + ": x=" + v.x + ", y=" + v.y + ",z=" + v.z + ",w=" + v.w);
	}
}

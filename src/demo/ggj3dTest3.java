package demo;


import gobiguy.ggj3d.*;
import gobiguy.ggjmath.*;

import java.awt.*;
import java.applet.*;
import java.util.*;
import java.net.*;

public class ggj3dTest3 extends Applet implements Runnable
{
	Thread thread;
	
	Image image;
	
	Image memoryImage;
	
	ggj3dComponent component1 = new ggj3dComponent();
	ggj3dComponent component2 = new ggj3dComponent();
	ggj3dObject object0 = new ObjectB();
	ggj3dObject object1 = new ObjectB();
	ggj3dObject object2 = new ObjectB();
	ggj3dObject object3 = new ObjectB();
	ggj3dContainer container1 = new ggj3dContainer();
	ggj3dContainer container2 = new ggj3dContainer();
	ggj3dContainer container3 = new ggj3dContainer();
	ggj3dScene scene1 = new ggj3dScene();
	
	ggj3dMaterial material1 = new ggj3dMaterial(0.5f, 0.5f, 0.2f, 4f);
	ggj3dTexture texture1 = new ggj3dTexture(0xff0000);
	ggj3dTexture texture2 = new ggj3dTexture(0x0000ff);
	ggj3dLight light1 = new ggj3dLight(0xffffffff);
	ggj3dEnvironment environment1 = new ggj3dEnvironment(0xffffffff);
	
	ggjVector p = new ggjVector(0, 0, 50);
	ggjVector u = new ggjVector(1, 0, 0);
	ggjVector v = new ggjVector(0, 1, 0);
	ggjVector w = new ggjVector(0, 0, -1);
	ggj3dMonitor monitor1 = new ggj3dMonitor();
	ggj3dPhoto photo1 = new ggj3dPhoto(300, 300);
	ggj3dSceneRender render1 = new ggj3dWareFrameRender2(0xff0000);

	public void init()
	{
		scene1.setEnvironment(environment1);
		scene1.add(light1);
	
		component1.setName("bu jian 1");
		component2.setName("bu jian 2");
		object0.setName("object 0"); /*object0.shift(3f, -3f, 0f); */object0.setTexture(texture1); object0.setMaterial(material1);
		object1.setName("object 1"); /*object1.shift(3f, 3f, 0f);*/ object1.setTexture(texture2); object1.setMaterial(material1);
		object2.setName("object 2"); /*object2.shift(4f, 4f, 0f);*/ object2.setTexture(texture1); object2.setMaterial(material1);
		object3.setName("object 3"); /*object3.shift(8f, 8f, 0f);*/ object3.setTexture(texture1); object3.setMaterial(material1);
		container1.setName("rong qi 1"); container1.shift(8f, 8f, 0f);
		container2.setName("rong qi 2"); container2.shift(8f, 8f, 0f);
		container3.setName("rong qi 3"); container3.shift(8f, 8f, 0f);
		scene1.setName("scene 1");
		
		monitor1.setPosition(p);
		monitor1.setAxises(u, v, w);
		monitor1.setLenFOV(60, 60);
		monitor1.setSceneRender(render1);
		monitor1.setCurrentPhoto(photo1);
		
		scene1.add(object0); System.out.println( "object0.getScene().getName()" + object0.getScene().getName() );
		scene1.add(container1);
			container1.add(object1); System.out.println( "object1.getScene().getName()" + object1.getScene().getName() );
			container1.add(container2);
				container2.add(object2); System.out.println( "object2.getScene().getName()" + object2.getScene().getName() );
				container2.add(container3);
					container3.add(object3); System.out.println( "object3.getScene().getName()" + object3.getScene().getName() );
					
	
	}
	
	public void start()
	{
		if (thread == null)
		{
			thread = new Thread(this);
			thread.start();
		}
	}
	
	public void stop()
	{
		if (thread != null)
		{
			thread = null;
		}
	}

	public void run()
	{
		while( thread != null)
		{
			repaint();
			try
			{
				//object0.rotatePosition(0f, 0f, 1f);
				container1.rotate(0f, 0f, 1f);
				container2.rotate(0f, 0f, 1f);
				container3.rotate(0f, 0f, 1f);
				//object2.rotate(0f, 0f, 4f);
				//monitor1.rotate(0f, 0f, 1f);
				//monitor1.shift(0f, 0f, -1f);
				thread.sleep(20);
			}
			catch (InterruptedException e)
			{
				System.out.println("interrupted");
			}
		}
	}

	public void update(Graphics g)
	{
		paint(g);
	}
	
	public void paint(Graphics g)
	{
		monitor1.shoot(scene1);
		
		ggj3dPhoto photo = monitor1.getCurrentPhoto();
		Dimension sz1 = getSize();
		memoryImage = ggj3dToolkit.getImage(photo);
		
		int left = sz1.width - photo.getWidth() >> 1;
		int top = sz1.height - photo.getHeight() >> 1;
		
		g.drawImage(memoryImage, left, top, this);
		
	}
	
	public boolean imageUpdate(Image image, int i, int j, int k, int l, int i1)
	{
	    return true;
	}
}


package demo;


import gobiguy.ggj3d.*;
import gobiguy.ggjmath.*;

import java.awt.*;
import java.applet.*;
import java.util.*;
import java.net.*;

public class ggj3dTest1 extends Applet implements Runnable
{
	Thread thread;
	
	Image image;
	
	Image memoryImage;
	
	ggj3dComponent component1 = new ggj3dComponent();
	ggj3dComponent component2 = new ggj3dComponent();
	ggj3dObject object0 = new ObjectB();
	ggj3dObject object1 = new ObjectB();
	ggj3dObject object2 = new ObjectB();
	ggj3dObject object3 = new ObjectA();
	ggj3dObject object4 = new ObjectA();
	ggj3dContainer container1 = new ggj3dContainer();


	ggj3dContainer container2 = new ggj3dContainer();
	ggj3dContainer container3 = new ggj3dContainer();


	ggj3dScene scene1 = new ggj3dScene();
	
	ggj3dMaterial material1 = new ggj3dMaterial(0.1f, 0.5f, 0.2f, 4f);
	ggj3dMaterial material2 = new ggj3dMaterial(0.1f, 0.3f, 0.2f, 4f);
	ggj3dTexture texture1 = new ggj3dTexture(0xffffff);
	ggj3dTexture texture2 = new ggj3dTexture(0xff0000);
	ggj3dTexture texture3;
	ggj3dLight light1 = new ggj3dLight(0xff0000);
	ggj3dEnvironment environment1 = new ggj3dEnvironment(0xffffffff);
	
	ggjVector p = new ggjVector(0, 0, 10);
	ggjVector u = new ggjVector(1, 0, 0);
	ggjVector v = new ggjVector(0, 1, 0);
	ggjVector w = new ggjVector(0, 0, -1);
	ggj3dMonitor monitor1 = new ggj3dMonitor();
	ggj3dPhoto photo1 = new ggj3dPhoto(300, 300);
	ggj3dSceneRender render1 = new ggj3dWareFrameRender2(0xff0000);
	ggj3dSceneRender render2 = new ggj3dGouraudRender2();

	public void init()
	{
		texture3 = new ggj3dTexture(getDocumentBase(), "../pictures/segara.jpg");
	
		scene1.setEnvironment(environment1);
		scene1.add(light1); light1.shift(5f, 5f, 20f);
		scene1.add(monitor1);
	
	
		component1.setName("bu jian 1");
		component2.setName("bu jian 2");
		object0.setName("object 0"); object0.shift(3f, -3f, 0f); object0.setTexture(texture3); object0.setMaterial(material1);
		object1.setName("object 1"); object1.shift(3f, 3f, 0f); object1.setTexture(texture3); object1.setMaterial(material2);
		object2.setName("object 2"); object2.shift(-3f, -3f, 0f); object2.setTexture(texture2); object2.setMaterial(material1);
		object3.setName("object 3"); object3.shift(-3f, 3f, 0f); object3.setTexture(texture3); object3.setMaterial(material2);
		object4.setName("object 4"); object4.shift(-2f, -2f, 0f); object4.setTexture(texture1); object4.setMaterial(material1);
		container1.setName("rong qi 1"); container1.shift(3f, -3f, 0f);
		container2.setName("rong qi 2"); container2.shift(2f, 2f, 0f);
		container3.setName("rong qi 3");
		scene1.setName("scene 1");
		
		monitor1.setPosition(p);
		monitor1.setAxises(u, v, w);
		monitor1.setLenFOV(100, 60);
		monitor1.setSceneRender(render2);
		monitor1.setCurrentPhoto(photo1);
		
		scene1.add(object0);
		scene1.add(object4);
		scene1.add(container1);
			container1.add(object3);
			container1.add(component1);
			container1.add(container2);
				container2.add(object2);
				container2.add(container3);
					container3.add(component2);
					container3.add(object1);
					
		
		ggj3dScene scene;
		scene = object1.getScene();
		
		scene = object1.getScene();
		monitor1.shoot(scene1);
	
	}
	
	public void start()
	{
		if (thread == null)
		{
			thread = new Thread(this);
			thread.start();
		}
	}
	
	public void stop()
	{
		if (thread != null)
		{
			thread = null;
		}
	}

	public void run()
	{
		while( thread != null)
		{
			repaint();
			try
			{
				object1.rotatePosition(0f, 0f, .3f);
				//object1.rotatePosition(0f, 1f, 0f);
				object0.rotatePosition(1f, 1f, 0f);
				//container1.rotateSelf(0f, 0f, -.3f);
				//container2.rotateSelf(0f, 1f, 0f);
				//object1.shift( (float)(.2d * Math.random()) - .1f, (float)(.2d * Math.random()) - .1f, (float)(.2d * Math.random()) - .1f );
				//object0.rotateSelf(0f, 0f, .3f);
				//object1.rotateSelf(0f, 1f,0f);
				object2.rotate(1f, 0f, 0f);
				object3.rotate(0f, 1f, 1f);
				object4.rotate(0f, 1f, 0f);
				//object2.rotate(0f, 0f, 4f);
				//monitor1.rotate(0f, 0f, 1f);
				//object4.shift(0f, 0f, -.2f);
				//monitor1.shift(0f, 0f, -1f);
				thread.sleep(10);
			}
			catch (InterruptedException e)
			{
				System.out.println("interrupted");
			}
		}
	}

	public void update(Graphics g)
	{
		paint(g);
	}
	
	public void paint(Graphics g)
	{
		monitor1.shoot(scene1);
		
		ggj3dPhoto photo = monitor1.getCurrentPhoto();
		Dimension sz1 = getSize();
		memoryImage = ggj3dToolkit.getImage(photo);
		
		int left = sz1.width - photo.getWidth() >> 1;
		int top = sz1.height - photo.getHeight() >> 1;
		
		g.drawImage(memoryImage, left, top, this);
		
	}
	
	public boolean imageUpdate(Image image, int i, int j, int k, int l, int i1)
	{
	    return true;
	}

	void print(String head, ggjVector v)
	{
		System.out.println(head + ": x=" + v.x + ", y=" + v.y + ",z=" + v.z + ",w=" + v.w);
	}
}

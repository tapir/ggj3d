****************************************************************
2002-08-16

ggj3d终于实现了它的最初的版本-version 0.01，

下一步可以做的工作：
- <improvement 1> 添加反混淆功能；
- <improvement 2> 使用一些加速算法；
- <improvement 3> 帧率统计；
- <improvement 4> 灯光设计需要加强，增加衰减；
- <improvement 5> 3ds文件的导入亦需加强，尤其是纹理的导入；
- <improvement 6> 三角形，物体拾取；
- <improvement 7> 鱼眼摄像；
- <improvement 8> 光线追踪算法；
- <improvement 9> 毛发处理；
- 做几个小的3d游戏；


****************************************************************
2002-08-21

<bug 1>
- Monitor.setSceneRender();
两个monitor不能使用一个scene render 

****************************************************************
2002-08-26
- bug1已修正

<unnatural 1>
- scene的设计不应该包括投影部分，投影应该是
sceneRender的责任。

<unnatural 2>
- 投影时，不需要每次都计算纹理坐标，因为大多数情况下，
  纹理坐标都是恒定的。

<bug 2>
- bug --- 不能从一个container中删除一个object，
原因和上一个bug差不多---死循环。

****************************************************************
2002-08-27

- unnatural 1 已经修正
- unnatural 2 已经修正
- 对于 unnatural 2，有很多问题需要探讨。
在directx中的渲染都是每个模型自己的任务，这样有很多优点，
a 各自的渲染并非完全一致，一致的只是最底层的操作（对三角形的渲染）。
至于本API，目前还未采用此设计。
b 不同object可以拥有统一份原始数据。

****************************************************************
2002-08-31
- improvement 1 finished(最简单的反混淆功能)。
代码有很多地方需要优化：
- <improvement 10>函数public,protected, private选择
- <improvement 11>对速度有影响的优化
还有一些需要改进的地方：
- <improvement 12>一些vector最好用hastable来代替，特别是
object vector
- <improvement 13>需要一个时间库，在统计帧率，代码效率测试，
帧率控制等方面应用。

***************************************************************
2002-09-01
- <improvement 14>因该做一个applet模板，类似于directx8。
- <improvement 15>component少scale函数。


1
< 总体设计 >

本api主要有两部分：模型组织，场景渲染。
另外还有一些辅助模块。

模型部分采用 component - cainter 结构。
渲染使用z缓冲算法，gauroud光线渲染算法。
辅助模块有：数学模块，文件输入输出模块，等。

模型组织部分：
3dComponent，3d部件，包括各种物体，灯光，摄像机等，
当然也包括3dContainer---3d容器。3dScene是一个容器。
一般一个小型程序只有一个Scene。

场景渲染部分：
SceneRender，渲染器，目前实现的有GauroudRender，WireFrameRender。
计划实现LightTraceRender等。

2
< 渲染过程 >

目前的渲染均在SceneRender中完成，这样做可以使设计简单明了，
但有不足的地方。

3
< 3dObject >

3dObject是基本的3d物体，它有以下属性：
- 纹理
- 材质
- 几何数据

这几种属性可以几个object同时共有，这个特点在以下场合很有用，
比如很多树，很多楼房，...


